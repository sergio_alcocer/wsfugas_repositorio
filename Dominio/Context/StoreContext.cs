﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Dominio.Entidades;
using Dominio.model;
using Dominio.Resultados;

namespace Dominio.Context
{
  public  class StoreContext : DbContext
    {

        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {

        }

        public DbSet<CAT_EMPLEADOS> CAT_EMPLEADOS { get; set; }
        public DbSet<APL_CAT_ROLES> APL_CAT_ROLES { get; set; }
        public DbSet<Cat_Cor_Brigadas> Cat_Cor_Brigadas { get; set; }
        public DbSet<CAT_COR_DISTRITOS> CAT_COR_DISTRITOS { get; set; }
        public DbSet<CAT_COR_TIPOS_FALLAS> CAT_COR_TIPOS_FALLAS { get; set; }
        public DbSet<Ope_Cor_Visitas_Ordenes_Trabajo> Ope_Cor_Visitas_Ordenes_Trabajo { get; set; }
        public DbSet<CAT_COM_PRODUCTOS> CAT_COM_PRODUCTOS { get; set; }
        public DbSet<OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES> OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES { get; set; }
        public DbSet<Ope_Cor_Asignacion_Distritos_Empleados> Ope_Cor_Asignacion_Distritos_Empleados { get; set; }
        public DbSet<Cat_Cor_Material_Fugas> Cat_Cor_Material_Fugas { get; set; }
        public DbSet<Cat_Cor_Diametro_Fugas> Cat_Cor_Diametro_Fugas { get; set; }
        public DbSet<Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados> Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados { get; set; }
        public DbSet<ope_cor_almacenamientos_imagenes> ope_cor_almacenamientos_imagenes { get; set; }
        public DbSet<Cat_Cor_Colonias> Cat_Cor_Colonias { get; set; }
        public DbSet<Cat_Cor_Calles> Cat_Cor_Calles { get; set; }
        public DbSet<Cat_Cor_Calles_Colonias> Cat_Cor_Calles_Colonias { get; set; }
        public DbSet<CAT_COR_VIALIDADES> CAT_COR_VIALIDADES { get; set; }

        [DbFunction("doLogin", "dbo")]
        public static int verificarEmpleado(string NO_EMPLEADO, string PASSW0RD)
        {
            throw new NotImplementedException();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Query<CoordenadasOrdenesModel>();
            modelBuilder.Query<OrdenTrabajoViewModel>();
            modelBuilder.Query<Cls_Totales>();
            modelBuilder.Query<EmpleadosOrdenTrabajoModel>();
            modelBuilder.Query<OrdenOTSeguimientoModel>();
            modelBuilder.Query<PrediosViewModel>();
            modelBuilder.Query<Cls_OT>();
        }

        
    }
}
