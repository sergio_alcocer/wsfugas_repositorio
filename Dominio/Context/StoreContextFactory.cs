﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Context
{
   public class StoreContextFactory : IDesignTimeDbContextFactory<StoreContext>
    {

        public StoreContext CreateDbContext(string[] args)
        {

            var optionsBuilder = new DbContextOptionsBuilder<StoreContext>();
            var connectionString = @"Server=172.16.0.115\MSSQLSERVER2016;Database=Sistema_Cuidado_Bebes_Migracion;User=jahn; Password=cntldes2018$;MultipleActiveResultSets=true;";

            Console.WriteLine(connectionString);

            optionsBuilder.UseSqlServer(connectionString, options => options.EnableRetryOnFailure());
            optionsBuilder.ConfigureWarnings(warnings => warnings.Throw(RelationalEventId.QueryClientEvaluationWarning));

            Console.WriteLine(connectionString);

            return new StoreContext(optionsBuilder.Options);
        }

    }
}
