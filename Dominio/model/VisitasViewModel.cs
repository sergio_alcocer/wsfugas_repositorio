﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class VisitasViewModel
    {

        public string No_Visita_Trabajo { get; set; }
        public string No_Orden_Trabajo { get; set; }
        public string Detalles { get; set; }
        public string Fecha { get; set; }
        public string Estatus { get; set; }
        public string Nombre_Usuario { get; set; }
    }
}
