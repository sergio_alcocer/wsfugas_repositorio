﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class OrdenOTSeguimientoModel
    {

        public string Brigada_ID { get; set; }
        public string Distrito_ID { get; set; }
        public string Tipo_Falla_ID { get; set; }
        public string Reporto { get; set; }
        public string Fecha_Trabajo { get; set; }
        public string Estatus { get; set; }
    }
}
