﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class CoordenadasOrdenesModel
    {
        public string No_Orden_Trabajo { get; set; }
        public decimal Longitud_Trabajo { get; set; }
        public decimal Latitud_Trabajo { get; set; }
    }
}
