﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
  public  class OrdenCreateModel
    {
        public string Calle_ID { get; set; }
        public string Calle_Referencia_ID { get; set; }
        public string Calle_Referencia_ID2 { get; set; }
        public string Colonia_ID { get; set; }
        public string Domicilio_Referencia { get; set; }
        public string Empleado_ID { get; set; }
        public string Nombre_Empleado { get; set; }
        public string Observaciones { get; set; }
        public string Predio_ID { get; set; }
        public string Referencia { get; set; }
        public string Reporto { get; set; }
        public string Tipo_Falla_ID { get; set; }
        public string Brigada_ID { get; set; }
        public string Distrito_ID { get; set; }
        public string Numero { get; set; }
        public string No_Cuenta { get; set; }
        public string No_Medidor { get; set; }
        public string Telefono { get; set; }
    }
}
