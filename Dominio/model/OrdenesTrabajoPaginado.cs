﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class OrdenesTrabajoPaginado
    {
       public int TotalElementos { get; set; }
        public List<OrdenTrabajoViewModel> ListaOrdenes { get; set; }

        public OrdenesTrabajoPaginado() {
            this.TotalElementos = 0;
            this.ListaOrdenes = new List<OrdenTrabajoViewModel>();
        }
    }
}
