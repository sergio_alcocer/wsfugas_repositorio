﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class OrdenTrabajoViewModel
    {

        public string No_Orden_Trabajo { get; set; }
        public string Folio { get; set; }
        public string Estatus { get; set; }
        public string Fecha_Elaboro { get; set; }
        public string No_Cuenta { get; set; }
        public string Colonia { get; set; }
        public string Tipo_Falla_ID { get; set; }
        public string Falla { get; set; }
        public string No_Suspension { get; set; }
        public string Predio_ID { get; set; }
        public string Clave_Nom { get; set; }
        public string Usuario { get; set; }
        public string Domicilio { get; set; }
        public string Brigada_ID { get; set; }
        public string Distrito_ID { get; set; }
        public string Distrito { get; set; }
        public string Observaciones { get; set; }
        public string RPU { get; set; }
        public string No_Medidor { get; set; }
        public decimal latitud { get; set; }
        public decimal longitud { get; set; }
        public string Calle_ID { get; set; }
        public string Colonia_ID { get; set; }
        public string Marca { get; set; }
        public decimal Ultima_Lectura { get; set; }
        public string Autorizo { get; set; }
        public string Fecha_Trabajo { get; set; }
        public string Observaciones_Trabajo_Realizado { get; set; }
        public string Recibio { get; set; }
        public string Reporto { get; set; }
        public int Material_Fuga_ID { get; set; }
        public int Diametro_Fuga_ID { get; set; }
        public string Fecha_Inicio { get; set; }
        public string Fecha_Termino { get; set; }

    }
}
