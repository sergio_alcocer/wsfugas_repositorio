﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
  public  class PrediosPaginado
    {
        public int TotalElementos { get; set; }
        public List<PrediosViewModel> ListaPredios { get; set; }

        public PrediosPaginado() {
            this.TotalElementos = 0;
            this.ListaPredios = ListaPredios;
        }

    }
}
