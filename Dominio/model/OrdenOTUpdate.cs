﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
  public  class OrdenOTUpdate
    {
        public string No_Orden_Trabajo { get; set; }
        public string Falla_ID { get; set; }
        public string Empleado_Realizo_ID { get; set; }
        public string Comentarios { get; set; }
        public string Anomalias { get; set; }
        public string Fecha_Trabajo { get; set; }
        public string Fecha_Inicio_Hora { get; set; }
        public string Fecha_Fin_Hora { get; set; }
        public string Trabajo_Realizado { get; set; }
        public string Diametro_Fuga_ID { get; set; }
        public string Material_Fuga_ID { get; set; }
        public string Nombre_Usuario { get; set; }
        public string BrigadaID { get; set; }
        public string Empleado_ID { get; set; }
        public string RPU { get; set; }
    }
}
