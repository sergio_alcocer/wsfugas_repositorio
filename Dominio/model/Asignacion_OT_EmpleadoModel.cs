﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class Asignacion_OT_EmpleadoModel
    {
        public string No_Asignacion { get; set; }
        public string Empleado_ID { get; set; }
        public string No_Orden_Trabajo { get; set; }
        public string Usuario_Creo { get; set; }
    }
}
