﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class FotoOTModel
    {
        public string Nombre_Foto { get; set; }
        public string Cadena_Foto { get; set; }
        public string RPU { get; set; }
        public string No_Orden_Trabajo { get; set; }
        public string Nombre_Usuario { get; set; }
        public string URL_Imagen { get; set; }
        public string Descripcion { get; set; }

        public FotoOTModel() {
            this.Nombre_Foto = "";
            this.Cadena_Foto = "";
            this.No_Orden_Trabajo = "";
            this.URL_Imagen = "";
            this.Nombre_Usuario = "";
            this.Descripcion = "";
            this.RPU = "";
        }
    }
}
