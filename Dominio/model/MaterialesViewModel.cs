﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class MaterialesViewModel
    {
        public string Producto_ID { get; set; }
        public string Unidad { get; set; }
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public decimal Costo { get; set; }
    }
}
