﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
    public class PrediosViewModel
    {

        public string RPU { get; set; }
        public string Predio_ID { get; set; }
        public string No_Cuenta { get; set; }
        public string Nombre { get; set; }
        public string Apellido_Paterno { get; set; }
        public string Apellido_Materno { get; set; }
        public string No_Zona { get; set; }
        public string Estatus { get; set; }
        public string Tipo { get; set; }
        public string Calle { get; set; }
        public string Colonia { get; set; }
        public string Numero_Exterior { get; set; }
        public string Numero_Interior { get; set; }
        public string Manzana { get; set; }
        public string Lote { get; set; }
        public string No_Medidor { get; set; }
        public string Calle_ID { get; set; }
        public string Colonia_ID { get; set; }
        public string Calle_Referencia1_ID { get; set; }
        public string Calle_Referencia2_ID { get; set; }
        public string Calle_Referencia { get; set; }
        public string Calle_Referencia2 { get; set; }
    }
}
