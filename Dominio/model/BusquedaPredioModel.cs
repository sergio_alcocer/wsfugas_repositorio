﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class BusquedaPredioModel
    {
        public int Pagina { get; set; }
        public string RPU { get; set; }
        public string No_Cuenta { get; set; }
        public string No_Medidor { get; set; }
        public string NombreUsuario { get; set; }
    }
}
