﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class EventoModel
    {
        public string RPU { get; set; }
        public string ID_Tabla { get; set; }
        public  string Nombre_Tabla { get; set; }
        public string Descripcion { get; set; }
        public string  Usuario_Creo { get; set; }
    }
}
