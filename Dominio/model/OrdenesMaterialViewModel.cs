﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class OrdenesMaterialViewModel
    {
        public string Ordenes_Trabajo_Detalles_Materiales_ID { set; get; }
        public string No_Orden_trabajo { get; set; }
        public string Producto_ID { get; set; }
        public double Cantidad { get; set; }
        public decimal CostoPorUnidad { get; set; }
        public decimal CostoTotal { get; set; }
        public string Nombre_Usuario { get; set; }
        public string Nombre_Material { get; set; }
        public string Unidad { get; set; }
    }
}
