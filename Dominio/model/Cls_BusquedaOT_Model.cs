﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.model
{
   public class Cls_BusquedaOT_Model
    {
         public string Query { get; set; }
         public string Distrito_ID { get; set; }
         public string Brigada_ID { get; set; }
         public int Pagina { get; set; }
         public string Empleado_ID { get; set; }
         public string Empleado_Asignado_ID { get; set; }
         public string Falla_ID { get; set; }
         public string Estatus { get; set; }
         public string Fecha_Inicio { get; set; }
        public string Fecha_Fin { get; set; }
    }
}
