﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("CAT_COR_TIPOS_FALLAS")]
    public class CAT_COR_TIPOS_FALLAS
    {
        [Key]
        public string TIPO_FALLA_ID { get; set; }
        public string BRIGADA_ID { get; set; }
        public string DISTRITO_ID { get; set; }
        public string ESTATUS { get; set; }
        public string DESCRIPCION { get; set; }

    }
}
