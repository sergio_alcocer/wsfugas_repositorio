﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Cat_Cor_Calles")]
    public class Cat_Cor_Calles
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CALLE_ID { get; set; }
        public string VIALIDAD_ID { get; set; }
        public string NOMBRE { get; set; }

        [ForeignKey(nameof(VIALIDAD_ID))]
        public CAT_COR_VIALIDADES CAT_COR_VIALIDADES { get; set; }

    }
}
