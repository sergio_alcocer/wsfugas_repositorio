﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("CAT_COR_DISTRITOS")]
    public class CAT_COR_DISTRITOS
    {
        [Key]
        public string DISTRITO_ID { get; set; }
        public string NOMBRE { get; set; }
        public string Empleado_ID { get; set; }
    }
}
