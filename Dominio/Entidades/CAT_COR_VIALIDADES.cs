﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("CAT_COR_VIALIDADES")]
    public class CAT_COR_VIALIDADES
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string VIALIDAD_ID { get; set; }
        public string NOMBRE { get; set; }
    }
}
