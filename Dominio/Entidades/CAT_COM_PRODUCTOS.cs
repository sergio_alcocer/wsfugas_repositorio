﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("CAT_COM_PRODUCTOS")]
    public class CAT_COM_PRODUCTOS
    {
        [Key]
        public string PRODUCTO_ID { get; set; }
        public string UNIDAD { get; set; }
        public string CLAVE { get; set; }
        public string NOMBRE { get; set; }
        public decimal COSTO { get; set; }
    }
}
