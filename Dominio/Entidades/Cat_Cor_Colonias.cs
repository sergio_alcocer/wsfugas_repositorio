﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Dominio.Entidades
{
    [Table("Cat_Cor_Colonias")]
    public class Cat_Cor_Colonias
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string COLONIA_ID { get; set; }
        public string NOMBRE { get; set; }
        public string CODIGO_POSTAL { get; set; }
    }
}
