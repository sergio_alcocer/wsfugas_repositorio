﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("APL_CAT_ROLES")]
    public  class APL_CAT_ROLES
    {
        [Key]
        public string ROL_ID { get; set; }
        public string NOMBRE { get; set; }
    }
}
