﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Cat_Cor_Calles_Colonias")]
    public  class Cat_Cor_Calles_Colonias
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CALLE_COLONIA_ID { get; set; }
        public string COLONIA_ID { get; set; }
        public string CALLE_ID { get; set; }


        [ForeignKey(nameof(COLONIA_ID))]
        public Cat_Cor_Colonias Cat_Cor_Colonias { get; set; }

        [ForeignKey(nameof(CALLE_ID))]
        public Cat_Cor_Calles Cat_Cor_Calles { get; set; }
    }
}
