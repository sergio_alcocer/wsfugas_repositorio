﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("ope_cor_almacenamientos_imagenes")]
    public class ope_cor_almacenamientos_imagenes
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 imagen_id { get; set; }
        public int no_cuenta { get; set; }
        public string tipo_operacion { get; set; }
        public string descripcion_imagen { get; set; }
        public string ubicacion_imagen { get; set; }
        public string nombre_imagen { get; set; }
        public string extension_imagen { get; set; }
        public string mime_imagen { get; set; }
        public string usuario_creo { get; set; }
        public DateTime fecha_creo { get; set; }
        public string ubicacion_fotografias { get; set; }
        public string url_imagen { get; set; }
        public string RPU { get; set; }
        public string Estatus { get; set; }
        public string No_Orden_Trabajo { get; set; }

    }
}
