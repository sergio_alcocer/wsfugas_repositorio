﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("CAT_EMPLEADOS")]
    public class CAT_EMPLEADOS
    {
        [Key]
        public string EMPLEADO_ID { get; set; }

        public string ROL_ID { get; set; }
        public string NO_EMPLEADO { get; set; }
        public string APELLIDO_PATERNO { get; set; }
        public string APELLIDO_MATERNO { get; set; }
        public string NOMBRE { get; set; }
        public string PASSWORD { get; set; }
        public string ESTATUS { get; set; }
        public string Token { get; set; }
        [ForeignKey(nameof(ROL_ID))]
        public APL_CAT_ROLES APL_CAT_ROLES { get; set; }
    }
}
