﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Cat_Cor_Material_Fugas")]
    public class Cat_Cor_Material_Fugas
    {
        [Key]
        public int Material_Fuga_ID { get; set; }
        public string Material_Fuga { get; set; }
        public string Estatus { get; set; }
    }
}
