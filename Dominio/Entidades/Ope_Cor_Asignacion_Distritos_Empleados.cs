﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Ope_Cor_Asignacion_Distritos_Empleados")]
    public  class Ope_Cor_Asignacion_Distritos_Empleados
    {
        [Key]
        public string No_Asignacion { get; set; }
        public string Empleado_ID { get; set; }
        public string Distrito_ID { get; set; }
        public bool Usuario_Corte { get; set; }

        [ForeignKey(nameof(Empleado_ID))]
        public CAT_EMPLEADOS CAT_EMPLEADOS { get; set; }
        [ForeignKey(nameof(Distrito_ID))]
        public CAT_COR_DISTRITOS CAT_COR_DISTRITOS { get; set; }
    }
}
