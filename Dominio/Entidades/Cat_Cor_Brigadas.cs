﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Dominio.Entidades
{
    [Table("Cat_Cor_Brigadas")]
    public  class Cat_Cor_Brigadas
    {
        [Key]
        public string Brigada_ID { get; set; }
        public  string Distrito_ID { get; set; }
        public string Empleado_ID { get; set; }
        public string Nombre_Brigada { get; set; }
        public string Clave_Brigada { get; set; }

        [ForeignKey(nameof(Distrito_ID))]
        public CAT_COR_DISTRITOS CAT_COR_DISTRITOS { get; set; }
    }
}
