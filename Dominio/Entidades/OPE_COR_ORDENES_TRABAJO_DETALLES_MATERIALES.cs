﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES")]
    public  class OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Ordenes_Trabajo_Detalles_Materiales_ID { get; set; }
        public string No_Orden_trabajo { get; set; }
        public string PRODUCTO_ID { get; set; }
        public double Cantidad { get; set; }
        public decimal COSTO_X_UNIDAD { get; set; }
        public decimal COSTO_TOTAL { get; set; }
        public string USUARIO_CREO { get; set; }
        public DateTime FECHA_CREO { get; set; }

        [ForeignKey(nameof(PRODUCTO_ID))]
        public CAT_COM_PRODUCTOS CAT_COM_PRODUCTOS { get; set; }

    }
}
