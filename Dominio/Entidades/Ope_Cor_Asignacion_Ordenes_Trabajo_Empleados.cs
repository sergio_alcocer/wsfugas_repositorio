﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados")]
    public class Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string No_Asignacion { get; set; }
        public  string Empleado_ID { get; set; }
        public string No_Orden_Trabajo { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
    }
}
