﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Cat_Cor_Diametro_Fugas")]
    public  class Cat_Cor_Diametro_Fugas
    {
        [Key]
        public int Diametro_Fuga_ID { get; set; }
        public string Diametro_Fuga { get; set; }
        public string Estatus { get; set; }
    }
}
