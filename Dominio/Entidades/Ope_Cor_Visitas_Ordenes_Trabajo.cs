﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio.Entidades
{
    [Table("Ope_Cor_Visitas_Ordenes_Trabajo")]
    public  class Ope_Cor_Visitas_Ordenes_Trabajo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string No_Visita_Trabajo { get; set; }
        public string No_Orden_Trabajo { get; set; }
        public string Detalles { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Creo { get; set; }
        public string Estatus { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }
    }
}
