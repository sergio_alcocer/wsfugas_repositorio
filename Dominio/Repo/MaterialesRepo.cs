﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;

namespace Dominio.Repo
{
    public class MaterialesRepo : IMaterialesRepo
    {
        public StoreContext Context;

        public MaterialesRepo(StoreContext context)
        {
            Context = context;
        }

        public IQueryable<CAT_COM_PRODUCTOS> CAT_COM_PRODUCTOS => Context.CAT_COM_PRODUCTOS;

        public List<MaterialesViewModel> Obtener_Materiales()
        {
            List<MaterialesViewModel> materialesViewModels = new List<MaterialesViewModel>();

            materialesViewModels = Context.CAT_COM_PRODUCTOS.OrderBy(x => x.NOMBRE)
                                        .Select(x => new MaterialesViewModel
                                        {
                                            Producto_ID = x.PRODUCTO_ID,
                                            Clave = x.CLAVE,
                                            Nombre = x.NOMBRE,
                                            Costo = x.COSTO,
                                            Unidad = x.UNIDAD
                                        }).ToList();

            return materialesViewModels;
        }
    }
}
