﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;

namespace Dominio.Repo
{
   public class TipoFallaRepo :ITipoFallaRepo
    {
        public StoreContext Context;

        public TipoFallaRepo(StoreContext context) {
            Context = context;
        }

        public IQueryable<CAT_COR_TIPOS_FALLAS> CAT_COR_TIPOS_FALLAS => Context.CAT_COR_TIPOS_FALLAS;

        public List<Cls_Combo> obtenerFallas()
        {
            List<Cls_Combo> listaCombo = Context.CAT_COR_TIPOS_FALLAS.OrderBy(x => x.DESCRIPCION)
                .Where(x => x.ESTATUS == "ACTIVA").Select(x => new Cls_Combo
                {
                    ID = x.TIPO_FALLA_ID,
                    Name = x.DESCRIPCION
                }).ToList();

            return listaCombo;
        }

        public List<Cls_Combo> Obtener_Fallas_Por_Distrito(string Distrito_ID)
        {
            List<Cls_Combo> listaCombo = Context.CAT_COR_TIPOS_FALLAS.OrderBy(x => x.DESCRIPCION)
              .Where(x => x.ESTATUS == "ACTIVA" && x.DISTRITO_ID == Distrito_ID ).Select(x => new Cls_Combo
              {
                  ID = x.TIPO_FALLA_ID,
                  Name = x.DESCRIPCION
              }).ToList();

            return listaCombo;
        }
    }
}
