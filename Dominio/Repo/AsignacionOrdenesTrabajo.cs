﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Context;
using Dominio.Interfaces;
using Dominio.model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Dominio.Entidades;

namespace Dominio.Repo
{
    public class AsignacionOrdenesTrabajo : IAsignacionOrdenesTrabajo
    {
        public void agregarAsignacionTrabajo(StoreContext context, Asignacion_OT_EmpleadoModel asignacion_OT)
        {
            string strvalorMaximo = context.Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados.Max(x => x.No_Asignacion);
            int intvalorMaximo = Convert.ToInt32(strvalorMaximo);
            string nuevoID = String.Format("{0:0000000000}", intvalorMaximo + 1);

            Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados ope_Cor_Asignacion_Ordenes_Trabajo_Empleados = new Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados();
            ope_Cor_Asignacion_Ordenes_Trabajo_Empleados.Empleado_ID = asignacion_OT.Empleado_ID;
            ope_Cor_Asignacion_Ordenes_Trabajo_Empleados.Fecha_Creo = DateTime.Now;
            ope_Cor_Asignacion_Ordenes_Trabajo_Empleados.No_Asignacion = nuevoID;
            ope_Cor_Asignacion_Ordenes_Trabajo_Empleados.No_Orden_Trabajo = asignacion_OT.No_Orden_Trabajo;
            ope_Cor_Asignacion_Ordenes_Trabajo_Empleados.Usuario_Creo = asignacion_OT.Usuario_Creo;

            context.Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados.Add(ope_Cor_Asignacion_Ordenes_Trabajo_Empleados);
            context.SaveChanges();

        }
    }
}
