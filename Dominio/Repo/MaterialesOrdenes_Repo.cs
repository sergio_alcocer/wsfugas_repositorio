﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;


namespace Dominio.Repo
{
  public  class MaterialesOrdenes_Repo : IMaterialesOrdenes_Repo
    {
        public StoreContext Context;

        public MaterialesOrdenes_Repo(StoreContext context) {
            Context = context;
        }

        public IQueryable<OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES> OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES => Context.OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES;

        public void Agregar_Material(OrdenesMaterialViewModel ordenesMaterialView)
        {
            string strvalorMaximo = Context.OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES.Max(x => x.Ordenes_Trabajo_Detalles_Materiales_ID);
            int intvalorMaximo = Convert.ToInt32(strvalorMaximo);
            string nuevoID = String.Format("{0:0000000000}", intvalorMaximo + 1);

            OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES oPE_COR_ORDENES_TRABAJOs = new OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES();

            oPE_COR_ORDENES_TRABAJOs.Ordenes_Trabajo_Detalles_Materiales_ID = nuevoID;
            oPE_COR_ORDENES_TRABAJOs.No_Orden_trabajo = ordenesMaterialView.No_Orden_trabajo;
            oPE_COR_ORDENES_TRABAJOs.PRODUCTO_ID = ordenesMaterialView.Producto_ID;
            oPE_COR_ORDENES_TRABAJOs.Cantidad = ordenesMaterialView.Cantidad;
            oPE_COR_ORDENES_TRABAJOs.COSTO_TOTAL = ordenesMaterialView.CostoTotal;
            oPE_COR_ORDENES_TRABAJOs.COSTO_X_UNIDAD = ordenesMaterialView.CostoPorUnidad;
            oPE_COR_ORDENES_TRABAJOs.USUARIO_CREO = ordenesMaterialView.Nombre_Usuario;
            oPE_COR_ORDENES_TRABAJOs.FECHA_CREO = DateTime.Now;

            Context.OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES.Add(oPE_COR_ORDENES_TRABAJOs);
            Context.SaveChanges();

        }

        public void Eliminar_Material(string Ordenes_Trabajo_Detalles_Materiales_ID)
        {
            OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES oPE_COR_ORDENES_TRABAJOs = Context.OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES.Find(Ordenes_Trabajo_Detalles_Materiales_ID);
            Context.OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES.Remove(oPE_COR_ORDENES_TRABAJOs);
            Context.SaveChanges();
        }

        public decimal Obtener_Costo_Orden(StoreContext context, string No_Orden_Trabajo)
        {
            return context.OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES.Where(x => x.No_Orden_trabajo == No_Orden_Trabajo).Sum(x => x.COSTO_TOTAL);
        }

        public List<OrdenesMaterialViewModel> Obtener_Material_Orden(string No_Orden_Trabajo)
        {
            List<OrdenesMaterialViewModel> ordenesMaterials = new List<OrdenesMaterialViewModel>();

            ordenesMaterials = Context.OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES.OrderBy(x => x.CAT_COM_PRODUCTOS.NOMBRE)
                 .Where(x => x.No_Orden_trabajo == No_Orden_Trabajo)
                 .Select(x => new OrdenesMaterialViewModel
                 {
                     Ordenes_Trabajo_Detalles_Materiales_ID = x.Ordenes_Trabajo_Detalles_Materiales_ID,
                     Nombre_Material = x.CAT_COM_PRODUCTOS.NOMBRE,
                     Unidad = x.CAT_COM_PRODUCTOS.UNIDAD,
                     Cantidad = x.Cantidad,
                     CostoPorUnidad = x.COSTO_X_UNIDAD,
                     CostoTotal = x.COSTO_TOTAL,
                     No_Orden_trabajo = x.No_Orden_trabajo,
                     Producto_ID = x.PRODUCTO_ID
                 }
                 ).ToList();

            return ordenesMaterials;
        }
    }
}
