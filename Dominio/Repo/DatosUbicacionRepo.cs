﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;

namespace Dominio.Repo
{
   public class DatosUbicacionRepo :IDatosUbicacionRepo
    {
        public StoreContext Context;

        public DatosUbicacionRepo(StoreContext context) {
            Context = context;
        }

        public List<Cls_Combo> Obtener_Calles_Por_Colonia(string Colonia_ID)
        {
            List<Cls_Combo> listaCalles = new List<Cls_Combo>();

            listaCalles = Context.Cat_Cor_Calles_Colonias.OrderBy(x => x.Cat_Cor_Calles.NOMBRE)
                            .Where(x => x.COLONIA_ID == Colonia_ID)
                            .Select(x => new Cls_Combo
                            {
                                ID= x.CALLE_ID,
                                Name = x.Cat_Cor_Calles.CAT_COR_VIALIDADES.NOMBRE + " " + x.Cat_Cor_Calles.NOMBRE

                            }).ToList();
            return listaCalles;
        }

        public List<Cls_Combo> Obtener_Colonias()
        {
            List<Cls_Combo> listaColonias = new List<Cls_Combo>();

            listaColonias = Context.Cat_Cor_Colonias.OrderBy(x => x.NOMBRE)
                         .Select(x => new Cls_Combo
                         {
                             ID = x.COLONIA_ID,
                             Name = x.NOMBRE
                         }).ToList();

            return listaColonias;
        }
    }
}
