﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;
using Dominio.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Dominio.Resultados;

namespace Dominio.Repo
{
    public class PrediosRepo : IPrediosRepo
    {
        public StoreContext Context;
        string strSql;

        public PrediosRepo(StoreContext context) {
            Context = context;
        }

        public List<PrediosViewModel> Obtener_Predios(BusquedaPredioModel busquedaPredioModel)
        {
            int offset = 0;
            int renglonesPorPagina = 20;

            offset = (busquedaPredioModel.Pagina - 1) * renglonesPorPagina;

            List<PrediosViewModel> ListaPredio = new List<PrediosViewModel>();


            strSql = " SELECT p.RPU AS [RPU] " +
                "  , p.Predio_ID " +
                " ,p.No_Cuenta AS [No_Cuenta] " +
                " ,isnull(u.NOMBRE, '') AS [Nombre] " +
                " ,isnull(u.APELLIDO_PATERNO, '') AS [Apellido_Paterno] " +
                " ,isnull(u.APELLIDO_MATERNO, '') AS [Apellido_Materno] " +
                " ,z.DESCRIPCION AS [No_Zona] " +
                " ,p.ESTATUS AS [Estatus] " +
                " ,v.NOMBRE AS [Tipo] " +
                " ,l.NOMBRE AS [Calle] " +
                " ,c.NOMBRE AS [Colonia] " +
                " ,p.NUMERO_EXTERIOR AS [Numero_Exterior] " +
                " ,p.NUMERO_INTERIOR AS [Numero_Interior] " +
                " ,p.Manzana AS [Manzana] " +
                " ,p.Lote AS [Lote] " +
                " ,isnull(m.NO_MEDIDOR, '') AS [No_Medidor] " +
                " ,l.CALLE_ID AS Calle_ID " +
                " ,c.COLONIA_ID AS Colonia_ID " +
                " ,isnull(p.Calle_Referencia1_ID, '') AS Calle_Referencia1_ID " +
                " ,isnull(p.Calle_Referencia2_ID, '') AS Calle_Referencia2_ID " +
                " ,ISNULL(( " +
                "   SELECT v.NOMBRE + ' ' + ca.NOMBRE " +
                "  FROM Cat_Cor_Calles ca " +
                " JOIN CAT_COR_VIALIDADES v ON v.VIALIDAD_ID = ca.VIALIDAD_ID " +
                " WHERE ca.CALLE_ID = p.Calle_Referencia1_ID " +
                " ), '') AS [Calle_Referencia] " +
                ",ISNULL(( " +
                " SELECT v.NOMBRE + ' ' + ca.NOMBRE " +
                " FROM Cat_Cor_Calles ca " +
                " JOIN CAT_COR_VIALIDADES v ON v.VIALIDAD_ID = ca.VIALIDAD_ID " +
                " WHERE ca.CALLE_ID = p.Calle_Referencia2_ID " +
                "), '') AS [Calle_Referencia2] " +
                " FROM Cat_Cor_Predios p " +
                " JOIN Cat_Cor_Usuarios u ON u.USUARIO_ID = p.Usuario_ID " +
                " LEFT JOIN Cat_Cor_Colonias c ON c.COLONIA_ID = p.COLONIA_ID " +
                " LEFT JOIN Cat_Cor_Calles l ON l.CALLE_ID = p.CALLE_ID " +
                " LEFT JOIN CAT_COR_ZONAS z ON z.ZONA_ID = c.ZONA_ID " +
                " LEFT JOIN CAT_COR_VIALIDADES v ON v.VIALIDAD_ID = l.VIALIDAD_ID " +
                " LEFT JOIN Cat_Cor_Predios_Medidores pm ON pm.PREDIO_ID = p.Predio_ID " +
                "   AND pm.FECHA_DESINSTALACION IS NULL " +
                " LEFT JOIN Cat_Cor_Medidores m ON m.MEDIDOR_ID = pm.MEDIDOR_ID " +
                " AND m.ESTATUS = 'ACTIVO' " +
                " WHERE p.predio_id IS NOT NULL ";

            if (!String.IsNullOrWhiteSpace(busquedaPredioModel.NombreUsuario)) {
                strSql += " AND (u.NOMBRE + ' ' + u.APELLIDO_PATERNO + ' ' + u.APELLIDO_MATERNO) collate SQL_Latin1_General_CP850_CI_AI LIKE '%" + busquedaPredioModel.NombreUsuario + "%' ";
            }

            if (!String.IsNullOrWhiteSpace(busquedaPredioModel.No_Medidor)) {
                strSql += " and m.NO_MEDIDOR  LIKE '%" + busquedaPredioModel.No_Medidor + "%' ";
            }

            if (!String.IsNullOrWhiteSpace(busquedaPredioModel.RPU)) {
                strSql += "  and p.RPU LIKE '%" + busquedaPredioModel.RPU + "%'";
            }

            if (!String.IsNullOrWhiteSpace(busquedaPredioModel.No_Cuenta)) {
                strSql += " and p.No_Cuenta LIKE '%" + busquedaPredioModel.No_Cuenta + "%'";
            }

            strSql += " ORDER BY  p.Numero_Exterior ";

            strSql += " OFFSET " + offset + " ROWS  ";
            strSql += " FETCH Next " + renglonesPorPagina + " ROWS ONLY ";

            ListaPredio = Context.Query<PrediosViewModel>().FromSql(strSql).ToList();

            return ListaPredio;
        }

        public PrediosPaginado Obtener_Predios_Paginado(BusquedaPredioModel busquedaPredioModel)
        {
            PrediosPaginado prediosPaginado = new PrediosPaginado();
            prediosPaginado.TotalElementos = Obtener_Total_Predios(busquedaPredioModel);
            prediosPaginado.ListaPredios = Obtener_Predios(busquedaPredioModel);

            return prediosPaginado;
        }

        public int Obtener_Total_Predios(BusquedaPredioModel busquedaPredioModel)
        {
            int resultado = 0;
            Cls_Totales cls_Totales = new Cls_Totales();

            strSql = " SELECT   count(*) as resultado " +
            " FROM Cat_Cor_Predios p " +
            " JOIN Cat_Cor_Usuarios u ON u.USUARIO_ID = p.Usuario_ID " +
            " LEFT JOIN Cat_Cor_Colonias c ON c.COLONIA_ID = p.COLONIA_ID " +
            " LEFT JOIN Cat_Cor_Calles l ON l.CALLE_ID = p.CALLE_ID " +
            " LEFT JOIN CAT_COR_ZONAS z ON z.ZONA_ID = c.ZONA_ID " +
            " LEFT JOIN CAT_COR_VIALIDADES v ON v.VIALIDAD_ID = l.VIALIDAD_ID " +
            " LEFT JOIN Cat_Cor_Predios_Medidores pm ON pm.PREDIO_ID = p.Predio_ID " +
            "   AND pm.FECHA_DESINSTALACION IS NULL " +
            " LEFT JOIN Cat_Cor_Medidores m ON m.MEDIDOR_ID = pm.MEDIDOR_ID " +
            " AND m.ESTATUS = 'ACTIVO' " +
            " WHERE p.predio_id IS NOT NULL ";

            if (!String.IsNullOrWhiteSpace(busquedaPredioModel.NombreUsuario))
            {
                strSql += " AND (u.NOMBRE + ' ' + u.APELLIDO_PATERNO + ' ' + u.APELLIDO_MATERNO) collate SQL_Latin1_General_CP850_CI_AI LIKE '%" + busquedaPredioModel.NombreUsuario + "%' ";
            }

            if (!String.IsNullOrWhiteSpace(busquedaPredioModel.No_Medidor))
            {
                strSql += " and m.NO_MEDIDOR  LIKE '%" + busquedaPredioModel.No_Medidor + "%' ";
            }

            if (!String.IsNullOrWhiteSpace(busquedaPredioModel.RPU))
            {
                strSql += "  and p.RPU LIKE '%" + busquedaPredioModel.RPU + "%'";
            }

            if (!String.IsNullOrWhiteSpace(busquedaPredioModel.No_Cuenta))
            {
                strSql += " and p.No_Cuenta LIKE '%" + busquedaPredioModel.No_Cuenta + "%'";
            }

            cls_Totales = Context.Query<Cls_Totales>().FromSql(strSql).FirstOrDefault();

            if (cls_Totales != null)
                resultado = cls_Totales.resultado;

            return resultado;
        }
    }
}
