﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;


namespace Dominio.Repo
{
    public class DistritoEmpleado_Repo : IDistritoEmpleado_Repo
    {
        public StoreContext Context;

        public DistritoEmpleado_Repo(StoreContext context)
        {
            Context = context;
        }

        public IQueryable<Ope_Cor_Asignacion_Distritos_Empleados> Ope_Cor_Asignacion_Distritos_Empleados => Context.Ope_Cor_Asignacion_Distritos_Empleados;

        public List<Cls_Combo> Obtener_Empleados_Por_Un_Distrito(string Distrito_ID)
        {
            List<Cls_Combo> listaDistritosEmpleados = new List<Cls_Combo>();

            listaDistritosEmpleados = Context.Ope_Cor_Asignacion_Distritos_Empleados.OrderBy(x => x.CAT_EMPLEADOS.NOMBRE)
                                      .Where(x => x.CAT_EMPLEADOS.ESTATUS.ToLower().Equals("activo") &&  x.Distrito_ID == Distrito_ID)
                                      .Select(x => new Cls_Combo
                                      {
                                          ID = x.Empleado_ID,
                                          Name = x.CAT_EMPLEADOS.NOMBRE + " " + (x.CAT_EMPLEADOS.APELLIDO_PATERNO == null ? "" : x.CAT_EMPLEADOS.APELLIDO_PATERNO) + " " + (x.CAT_EMPLEADOS.APELLIDO_MATERNO == null ? "" : x.CAT_EMPLEADOS.APELLIDO_MATERNO)
                                      }).ToList();


            return listaDistritosEmpleados;
        }

        public List<Cls_Combo> Obtener_Empleado_Por_Brigadas(string Brigada_ID)
        {
            List<Cls_Combo> listaEmpleadoPorDistristo = new List<Cls_Combo>();

            var arregloDistritos = Context.Cat_Cor_Brigadas.Where(x => x.Brigada_ID == Brigada_ID).Select(x => x.Distrito_ID).ToArray();

            listaEmpleadoPorDistristo = Context.Ope_Cor_Asignacion_Distritos_Empleados.OrderBy(x => x.CAT_EMPLEADOS.NOMBRE)
                                    .Where(x => x.CAT_EMPLEADOS.ESTATUS.ToLower().Equals("activo")
                                     && arregloDistritos.Contains(x.Distrito_ID))
                                    .Select(x => new Cls_Combo
                                    {
                                        ID = x.Empleado_ID,
                                        Name = x.CAT_EMPLEADOS.NOMBRE + " " + (x.CAT_EMPLEADOS.APELLIDO_PATERNO == null ? "" : x.CAT_EMPLEADOS.APELLIDO_PATERNO) + " " + (x.CAT_EMPLEADOS.APELLIDO_MATERNO == null ? "" : x.CAT_EMPLEADOS.APELLIDO_MATERNO)
                                    }).ToList();


            return listaEmpleadoPorDistristo;
        }

        public List<Cls_Combo> Obtener_Todos_Los_Empleados_Distritos()
        {
            List<Cls_Combo> listaDistritosEmpleados = new List<Cls_Combo>();

            listaDistritosEmpleados = Context.Ope_Cor_Asignacion_Distritos_Empleados.OrderBy(x => x.CAT_EMPLEADOS.NOMBRE)
                                      .Where(x => x.CAT_EMPLEADOS.ESTATUS.ToLower().Equals("activo"))
                                      .Select(x => new Cls_Combo
                                      {
                                          ID = x.Empleado_ID,
                                          Name = x.CAT_EMPLEADOS.NOMBRE + " " + (x.CAT_EMPLEADOS.APELLIDO_PATERNO == null ? "" : x.CAT_EMPLEADOS.APELLIDO_PATERNO ) + " " +( x.CAT_EMPLEADOS.APELLIDO_MATERNO == null ? "": x.CAT_EMPLEADOS.APELLIDO_MATERNO)
                                      }).ToList();


            return listaDistritosEmpleados;
        }
    }
}
