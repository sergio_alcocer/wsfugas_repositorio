﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;


namespace Dominio.Repo
{
   public class DiametroFuega_Repo : IDiametrosFuga
    {

        public StoreContext Context;

        public DiametroFuega_Repo(StoreContext context) {
            Context = context;
        }

        public IQueryable<Cat_Cor_Diametro_Fugas> Cat_Cor_Diametro_Fugas => Context.Cat_Cor_Diametro_Fugas;

        public List<Cls_Combo> Obtener_Diametros_Fugas()
        {
            List<Cls_Combo> listaCombo = Context.Cat_Cor_Diametro_Fugas.OrderBy(x => x.Diametro_Fuga)
                                   .Where(x => x.Estatus == "ACTIVO")
                                   .Select(x => new Cls_Combo
                                   {
                                       ID = x.Diametro_Fuga_ID.ToString(),
                                       Name = x.Diametro_Fuga
                                   }).ToList();
            return listaCombo;
        }
    }
}
