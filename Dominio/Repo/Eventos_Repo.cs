﻿using System;
using System.Collections.Generic;
using System.Text;

using Dominio.Context;
using Dominio.Interfaces;
using Dominio.model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Dominio.Entidades;
using System.Data.SqlClient;
using System.Data;

namespace Dominio.Repo
{
    public class Eventos_Repo : IEventos
    {
        private string strSql;

        public void agregarEventos(StoreContext context, EventoModel eventoModel)
        {
            strSql = " INSERT INTO Ope_Cor_Eventos (No_Evento,No_Cuenta,ID_Tabla,Nombre_Tabla,Descripcion,Fecha,estatus_evento,Usuario_Creo,Fecha_Creo) values " +
               "( " +
               "(SELECT RIGHT('0000000000' + CAST(ISNULL(MAX( No_Evento), 0) + 1 AS VARCHAR(10)), 10) FROM Ope_Cor_Eventos WHERE No_Cuenta = " + eventoModel.RPU + "), " +
               "@rpu,@tabla_id,@nombre_tabla,@observacion,getdate(),'ACTIVO',@usuario,getdate())";

            context.Database.ExecuteSqlCommand(strSql,
                 new SqlParameter("@rpu", eventoModel.RPU),
                 new SqlParameter("@tabla_id", eventoModel.ID_Tabla),
                 new SqlParameter("@nombre_tabla", eventoModel.Nombre_Tabla),
                 new SqlParameter("@observacion", eventoModel.Descripcion),
                 new SqlParameter("@usuario", eventoModel.Usuario_Creo)
                );
              
        }
    }
}
