﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;

namespace Dominio.Repo
{
   public class Imagenes_Repo : IImagenesRepo
    {

        public StoreContext Context;

        public Imagenes_Repo(StoreContext context) {
            Context = context;
        }

        public IQueryable<ope_cor_almacenamientos_imagenes> ope_cor_almacenamientos_imagenes => Context.ope_cor_almacenamientos_imagenes;

        public void agregarImagen(FotoOTModel fotoOT, string ruta_http, string ubicacion_fisica)
        {
            ope_cor_almacenamientos_imagenes ope_Cor_Almacenamientos_Imagenes = new ope_cor_almacenamientos_imagenes();

            ope_Cor_Almacenamientos_Imagenes.Estatus = "ACTIVO";
            ope_Cor_Almacenamientos_Imagenes.extension_imagen = ".jpg";
            ope_Cor_Almacenamientos_Imagenes.mime_imagen = "image/jpeg";
            ope_Cor_Almacenamientos_Imagenes.fecha_creo = DateTime.Now;
            ope_Cor_Almacenamientos_Imagenes.no_cuenta = 0;
            ope_Cor_Almacenamientos_Imagenes.No_Orden_Trabajo = fotoOT.No_Orden_Trabajo;
            ope_Cor_Almacenamientos_Imagenes.RPU = fotoOT.RPU;
            ope_Cor_Almacenamientos_Imagenes.ubicacion_fotografias = ubicacion_fisica;
            ope_Cor_Almacenamientos_Imagenes.ubicacion_imagen = ubicacion_fisica;
            ope_Cor_Almacenamientos_Imagenes.tipo_operacion = "ORDENES TRABAJO";
            ope_Cor_Almacenamientos_Imagenes.usuario_creo = fotoOT.Nombre_Usuario;
            ope_Cor_Almacenamientos_Imagenes.nombre_imagen = fotoOT.Nombre_Foto;
            ope_Cor_Almacenamientos_Imagenes.descripcion_imagen = "imagen tomada en app fugas";
            ope_Cor_Almacenamientos_Imagenes.url_imagen = ruta_http + fotoOT.Nombre_Foto;

            Context.ope_cor_almacenamientos_imagenes.Add(ope_Cor_Almacenamientos_Imagenes);
            Context.SaveChanges();
        }

        public List<FotoOTModel> obtenerFotos(string no_orden_trabajo)
        {
            List<FotoOTModel> listaFotoModel = new List<FotoOTModel>();

            listaFotoModel = Context.ope_cor_almacenamientos_imagenes.Where(x => x.No_Orden_Trabajo == no_orden_trabajo
              && x.tipo_operacion == "ORDENES TRABAJO" && x.Estatus == "ACTIVO")
              .OrderByDescending(x => x.fecha_creo)
              .Select(x => new FotoOTModel
              {
                  URL_Imagen = x.url_imagen,
                  RPU = x.RPU,
                  Nombre_Foto = x.nombre_imagen,
                  Descripcion = x.descripcion_imagen,
                  No_Orden_Trabajo = x.No_Orden_Trabajo
              }).ToList();

            return listaFotoModel;
        }
    }
}
