﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;
using System.Globalization;

namespace Dominio.Repo
{
  public  class Visita_Repo : IVisitas_Repo
    {
        public StoreContext Context;

        public Visita_Repo(StoreContext context) {
            Context = context;
        }

        public IQueryable<Ope_Cor_Visitas_Ordenes_Trabajo> Ope_Cor_Visitas_Ordenes_Trabajo => Context.Ope_Cor_Visitas_Ordenes_Trabajo;

        public void Agregar_Visita(VisitasViewModel visitasView)
        {
            string strvalorMaximo = Context.Ope_Cor_Visitas_Ordenes_Trabajo.Max(x => x.No_Visita_Trabajo);
            int intvalorMaximo = Convert.ToInt32(strvalorMaximo);
            string nuevoID = String.Format("{0:0000000000}",  intvalorMaximo + 1);
            
            DateTime datFecha = DateTime.ParseExact(visitasView.Fecha, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            Ope_Cor_Visitas_Ordenes_Trabajo ope_Cor_Visitas = new Ope_Cor_Visitas_Ordenes_Trabajo();

            ope_Cor_Visitas.No_Visita_Trabajo = nuevoID;
            ope_Cor_Visitas.No_Orden_Trabajo = visitasView.No_Orden_Trabajo;
            ope_Cor_Visitas.Detalles = visitasView.Detalles;
            ope_Cor_Visitas.Fecha = datFecha;
            ope_Cor_Visitas.Fecha_Creo = DateTime.Now;
            ope_Cor_Visitas.Usuario_Creo = visitasView.Nombre_Usuario;
            ope_Cor_Visitas.Estatus = visitasView.Estatus;

            Context.Ope_Cor_Visitas_Ordenes_Trabajo.Add(ope_Cor_Visitas);
            Context.SaveChanges();
            

        }

        public void Eliminar_Visita(string pNoVistaTrabajo, string pUsuario)
        {
            Ope_Cor_Visitas_Ordenes_Trabajo ope_Cor_Visitas_Ordenes = Context.Ope_Cor_Visitas_Ordenes_Trabajo.Find(pNoVistaTrabajo);
            ope_Cor_Visitas_Ordenes.Estatus = "INACTIVO";
            ope_Cor_Visitas_Ordenes.Fecha_Modifico = DateTime.Now;
            ope_Cor_Visitas_Ordenes.Usuario_Modifico = pUsuario;

            Context.Update(ope_Cor_Visitas_Ordenes);
            Context.SaveChanges();
            
        }

        public List<VisitasViewModel> Obtener_Visitas(string pNoOrdenTrabajo)
        {
            List<VisitasViewModel> visitasViewModels = new List<VisitasViewModel>();

            visitasViewModels = Context.Ope_Cor_Visitas_Ordenes_Trabajo.OrderBy(x => x.Fecha)
                                .Where(x=> x.Estatus == "ACTIVO" && x.No_Orden_Trabajo == pNoOrdenTrabajo)
                                .AsEnumerable()  
                                 .Select(x => new VisitasViewModel
                                 {
                                     No_Visita_Trabajo = x.No_Visita_Trabajo,
                                     Detalles = x.Detalles,
                                     Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                                     No_Orden_Trabajo = x.No_Orden_Trabajo,
                                     Estatus = x.Estatus

                                 }).ToList();


            return visitasViewModels;
        }
    }
}
