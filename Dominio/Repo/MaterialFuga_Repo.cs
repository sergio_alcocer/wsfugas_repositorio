﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;

namespace Dominio.Repo
{
   public class MaterialFuga_Repo : IMaterialeFuga
    {
        public StoreContext Context;

        public MaterialFuga_Repo(StoreContext context) {
            Context = context;
        }

        public IQueryable<Cat_Cor_Material_Fugas> Cat_Cor_Material_Fugas => Context.Cat_Cor_Material_Fugas;

        public List<Cls_Combo> Obtener_Materiales_Fuga()
        {
            List<Cls_Combo> listaCombo = Context.Cat_Cor_Material_Fugas.OrderBy(x => x.Material_Fuga)
                                            .Where(x => x.Estatus == "ACTIVO")
                                            .Select(x => new Cls_Combo
                                            {
                                                ID = x.Material_Fuga_ID.ToString(),
                                                Name = x.Material_Fuga
                                            }).ToList();
            return listaCombo;
        }
    }
}
