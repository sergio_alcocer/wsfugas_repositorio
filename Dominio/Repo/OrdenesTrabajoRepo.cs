﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;
using Microsoft.EntityFrameworkCore;
using Dominio.util;
using System.Data.SqlClient;
using System.Data;
using Dominio.Resultados;
using Microsoft.EntityFrameworkCore.Storage;
using System.Globalization;

namespace Dominio.Repo
{
    public class OrdenesTrabajoRepo : IOrdenesTrabajoRepo
    {
        public StoreContext Context;
        private IAsignacionOrdenesTrabajo asignacionOrdenesTrabajo_Repo;
        private IEventos eventosRepo;
        private IMaterialesOrdenes_Repo materialesOrdenes_Repo;
        private ITipoFallaRepo tipoFallaRepo;
        private string strSql;


        public OrdenesTrabajoRepo(StoreContext context, IAsignacionOrdenesTrabajo asignacionOrdenes
            , IEventos eventosOT, IMaterialesOrdenes_Repo materialesOrdenes, ITipoFallaRepo tipoFallaRepositorio) {
            Context = context;
            asignacionOrdenesTrabajo_Repo = asignacionOrdenes;
            eventosRepo = eventosOT;
            materialesOrdenes_Repo = materialesOrdenes;
            tipoFallaRepo = tipoFallaRepositorio;
            Context.Database.SetCommandTimeout(200);
        }

        public void Actualizar_Coordenadas_Trabajo(CoordenadasOrdenesModel coordenadas)
        {
            strSql = "UPDATE OPE_COR_ORDENES_TRABAJO SET Longitud_Trabajo=@longitud, Latitud_Trabajo=@latitud WHERE No_Orden_Trabajo=@orden_trabajo";

            Context.Database.ExecuteSqlCommand(strSql,
                 new SqlParameter("@longitud", coordenadas.Longitud_Trabajo),
                 new SqlParameter("@latitud", coordenadas.Latitud_Trabajo),
                 new SqlParameter("@orden_trabajo", coordenadas.No_Orden_Trabajo)

                );

        }


        private void actualizarAsignacionOT(StoreContext pcontext, string pNo_Orden_Trabajo, string pUsuarioCreo)
        {


            strSql = "UPDATE ope_cor_ordenes_trabajo set " +
                   "Estatus = @estatus " +
                   ",Usuario_Modifico = @usuario_modifico " +
                   ",Fecha_Modifico = getdate() " +
                   "WHERE No_Orden_Trabajo= @no_orden_trabajo ";

            SqlParameter sqlParametroEstatus = new SqlParameter("@estatus", "ASIGNADA");
            SqlParameter sqlParametroUsuario = new SqlParameter("@usuario_modifico", pUsuarioCreo);
            SqlParameter salParametroNoOrdenTrabajo = new SqlParameter("@no_orden_trabajo", pNo_Orden_Trabajo);

            pcontext.Database.ExecuteSqlCommand(strSql, sqlParametroEstatus, sqlParametroUsuario, salParametroNoOrdenTrabajo);

        }

        private void actualizarOT(StoreContext pcontext, OrdenOTUpdate ordenOTUpdate, decimal costoMaterial)
        {
            string horaInicio;
            string horaFinal;
            string minutoInicio;
            string minutoFin;

            SqlParameter sqlParameterNoOrdenTrabajo = new SqlParameter("@no_orden_trabajo", SqlDbType.Char, 10);

            SqlParameter sqlParameterFechaTrabajo = new SqlParameter("@fecha_trabajo", SqlDbType.DateTime);
            SqlParameter sqlParameterFechaInicio = new SqlParameter("@fecha_trabajo_inicio", SqlDbType.DateTime);
            SqlParameter sqlParameterFechaFinal = new SqlParameter("@fecha_trabajo_final", SqlDbType.DateTime);

            SqlParameter sqlParameterHoraInicio = new SqlParameter("@hora_inicio", SqlDbType.Char, 2);
            SqlParameter sqlParameterHoraFinal = new SqlParameter("@hora_final", SqlDbType.Char, 2);

            SqlParameter sqlParameterMinutoInicio = new SqlParameter("@minuto_inicial", SqlDbType.Char, 2);
            SqlParameter sqlParameterMinutoFinal = new SqlParameter("@minuto_final", SqlDbType.Char, 2);

            SqlParameter sqlParameterBrigadaID = new SqlParameter("@brigada_id", SqlDbType.Char, 5);
            SqlParameter sqlParameterTrabajoRealizadoID = new SqlParameter("@trabajo_realizado_id", SqlDbType.Char, 5);
            SqlParameter sqlParameterTrabajoFallaID = new SqlParameter("@falla_id", SqlDbType.Char, 5);
            SqlParameter sqlParameterEmpleadoRealizoID = new SqlParameter("@empleado_realizo_id", SqlDbType.Char, 10);
            SqlParameter sqlParameterDiametroFugaID = new SqlParameter("@diametro_fuga_id", SqlDbType.Int);
            SqlParameter sqlParameterMaterialFuagaID = new SqlParameter("@material_fuga_id", SqlDbType.Int);

            SqlParameter sqlParameterTrabajoRealizadoTexto = new SqlParameter("@trabajo_realizado_texto", SqlDbType.VarChar);
            SqlParameter sqlParameterUsuarioModifico = new SqlParameter("@usuario_modifco", SqlDbType.VarChar);
            SqlParameter sqlParameterEstatus = new SqlParameter("@estatus", SqlDbType.VarChar);

            SqlParameter sqlParameterAnomalia = new SqlParameter("@anomalia", SqlDbType.VarChar);
            SqlParameter sqlParameterComentario = new SqlParameter("@comentario", SqlDbType.VarChar);

            SqlParameter sqlParameterCosto = new SqlParameter("@costo", SqlDbType.Decimal);

    
            DateTime datFechaTrabajo = DateTime.ParseExact(ordenOTUpdate.Fecha_Trabajo, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            DateTime datFechaInicio = DateTime.ParseExact(ordenOTUpdate.Fecha_Inicio_Hora, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            DateTime datFechaFin = DateTime.ParseExact(ordenOTUpdate.Fecha_Fin_Hora, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);

            horaInicio = datFechaInicio.Hour.ToString();
            minutoInicio = datFechaInicio.Minute.ToString();

            horaFinal = datFechaFin.Hour.ToString();
            minutoFin = datFechaFin.Minute.ToString();



            strSql = " UPDATE Ope_Cor_Ordenes_Trabajo SET Estatus = @estatus, Trabajo_Realizado=@trabajo_realizado_texto, " +
                     " Observaciones_Trabajo_Realizado = @comentario, anomalias_ot=@anomalia, Fecha_Trabajo=@fecha_trabajo, " +
                     " Brigada_ID=@brigada_id, Trabajo_Realizado_ID=@trabajo_realizado_id,Tipo_Falla_ID=@falla_id, " +
                     " Fecha_Inicio=@fecha_trabajo_inicio,Hora_Inicio=@hora_inicio,Minutos_Inicio=@minuto_inicial, " +
                     " Fecha_Termino =@fecha_trabajo_final,Hora_Termino=@hora_final,Minutos_Termino=@minuto_final, " +
                     " Usuario_Modifico = @usuario_modifco, Fecha_Modifico=getdate(), Material_Fuga_ID =@material_fuga_id, " +
                     " Diametro_Fuga_ID= @diametro_fuga_id, COSTO_MATERIAL_UTILIZADO=@costo, Empleado_ID_Realizo_Trabajo=@empleado_realizo_id WHERE No_Orden_Trabajo =@no_orden_trabajo";

            sqlParameterNoOrdenTrabajo.Value = ordenOTUpdate.No_Orden_Trabajo;

            if (ordenOTUpdate.Trabajo_Realizado == "SI") {
                sqlParameterEstatus.Value = "TERMINADA";
                sqlParameterFechaInicio.Value = datFechaInicio.Date;
                sqlParameterHoraInicio.Value = horaInicio;
                sqlParameterMinutoInicio.Value = minutoInicio;
                sqlParameterFechaFinal.Value = datFechaFin.Date;
                sqlParameterHoraFinal.Value = horaFinal;
                sqlParameterMinutoFinal.Value = minutoFin;
            }
            else
            {
                sqlParameterEstatus.Value = "ATENDIDA";
                sqlParameterFechaInicio.Value = DBNull.Value;
                sqlParameterHoraInicio.Value = DBNull.Value;
                sqlParameterMinutoInicio.Value = DBNull.Value;
                sqlParameterFechaFinal.Value = DBNull.Value;
                sqlParameterHoraFinal.Value = DBNull.Value;
                sqlParameterMinutoFinal.Value = DBNull.Value;
            }

            sqlParameterBrigadaID.Value = ordenOTUpdate.BrigadaID;
            sqlParameterComentario.Value = ordenOTUpdate.Comentarios;
            sqlParameterAnomalia.Value = ordenOTUpdate.Anomalias;
            sqlParameterFechaTrabajo.Value = datFechaTrabajo;
            sqlParameterTrabajoRealizadoTexto.Value = ordenOTUpdate.Trabajo_Realizado;
            sqlParameterTrabajoRealizadoID.Value = ordenOTUpdate.Falla_ID;
            sqlParameterTrabajoFallaID.Value = ordenOTUpdate.Falla_ID;


            sqlParameterUsuarioModifico.Value = ordenOTUpdate.Nombre_Usuario;

            if (ordenOTUpdate.Material_Fuga_ID.Length > 0 && ordenOTUpdate.Material_Fuga_ID != "-1")
                sqlParameterMaterialFuagaID.Value = ordenOTUpdate.Material_Fuga_ID;
            else
                sqlParameterMaterialFuagaID.Value = DBNull.Value;

            if (ordenOTUpdate.Diametro_Fuga_ID.Length > 0 && ordenOTUpdate.Diametro_Fuga_ID != "-1")
                sqlParameterDiametroFugaID.Value = ordenOTUpdate.Diametro_Fuga_ID;
            else
                sqlParameterDiametroFugaID.Value = DBNull.Value;


            if (costoMaterial == 0)
                sqlParameterCosto.Value = DBNull.Value;
            else
                sqlParameterCosto.Value = costoMaterial;

            if (ordenOTUpdate.Empleado_Realizo_ID.Trim().Length > 0 && ordenOTUpdate.Empleado_Realizo_ID != "-1")
                sqlParameterEmpleadoRealizoID.Value = ordenOTUpdate.Empleado_Realizo_ID;
            else
                sqlParameterEmpleadoRealizoID.Value = DBNull.Value;
            

            pcontext.Database.ExecuteSqlCommand(strSql,
                sqlParameterEstatus, sqlParameterTrabajoRealizadoTexto,
                sqlParameterComentario, sqlParameterAnomalia, sqlParameterFechaTrabajo,
                sqlParameterBrigadaID, sqlParameterTrabajoRealizadoID, sqlParameterTrabajoFallaID,
                sqlParameterFechaInicio, sqlParameterHoraInicio, sqlParameterMinutoInicio,
                sqlParameterFechaFinal, sqlParameterHoraFinal, sqlParameterMinutoFinal,
                sqlParameterUsuarioModifico, sqlParameterMaterialFuagaID, sqlParameterDiametroFugaID,
                sqlParameterCosto,
                sqlParameterEmpleadoRealizoID,
                sqlParameterNoOrdenTrabajo);


        }


        public string Actualizar_Orden_Trabajo(OrdenOTUpdate ordenOTUpdate)
        {
            string respuesta = "bien";
            decimal costoMaterialUtilizado;
            using (IDbContextTransaction transaction = Context.Database.BeginTransaction())
            {
                try
                {

                    if (ordenOTUpdate.Trabajo_Realizado == "SI")
                    {

                        Asignacion_OT_EmpleadoModel asignacion_OT_Empleado = new Asignacion_OT_EmpleadoModel();
                        asignacion_OT_Empleado.Empleado_ID = ordenOTUpdate.Empleado_Realizo_ID;
                        asignacion_OT_Empleado.No_Orden_Trabajo = ordenOTUpdate.No_Orden_Trabajo;
                        asignacion_OT_Empleado.Usuario_Creo = ordenOTUpdate.Nombre_Usuario;
                        asignacionOrdenesTrabajo_Repo.agregarAsignacionTrabajo(Context, asignacion_OT_Empleado);

                        if (ordenOTUpdate.RPU.Trim().Length > 0) {

                            EventoModel evento = new EventoModel();
                            evento.RPU = ordenOTUpdate.RPU;
                            evento.ID_Tabla = ordenOTUpdate.No_Orden_Trabajo;
                            evento.Nombre_Tabla = "OPE_COR_ORDENES_TRABAJO";
                            evento.Usuario_Creo = ordenOTUpdate.Nombre_Usuario;
                            evento.Descripcion = "Observaciones : \t" + ordenOTUpdate.Comentarios.Trim();

                            eventosRepo.agregarEventos(Context, evento);

                        }
                    }

                    costoMaterialUtilizado = materialesOrdenes_Repo.Obtener_Costo_Orden(Context, ordenOTUpdate.No_Orden_Trabajo);

                    actualizarOT(Context, ordenOTUpdate, costoMaterialUtilizado);

                    transaction.Commit();
                    respuesta = "bien";

                }
                catch (Exception ex) {

                    transaction.Rollback();
                    respuesta = ex.Message;
                }

            }

                return respuesta;
        }

        public List<EmpleadosOrdenTrabajoModel> obtenerEmpleadosOrdenTrabajo(string brigada_id, bool usuario_corte)
        {
            List<EmpleadosOrdenTrabajoModel> listadoEmpleados = new List<EmpleadosOrdenTrabajoModel>();


            strSql = "SELECT EM.Empleado_ID as ID " +
                    ",EM.NO_EMPLEADO as No_Empleado " +
                    ",ISNULL(EM.NOMBRE, '') + ' ' + ISNULL(EM.APELLIDO_PATERNO, '') + ' ' + ISNULL(EM.APELLIDO_MATERNO, '') AS Name " +
                "FROM Ope_Cor_Asignacion_Distritos_Empleados A " +
                "INNER JOIN Cat_Empleados EM ON A.Empleado_ID = EM.EMPLEADO_ID " +
                "INNER JOIN Cat_Cor_Brigadas B ON A.Distrito_ID = B.Distrito_ID " +
                "WHERE 1 = 1 ";
                
               if(!String.IsNullOrWhiteSpace(brigada_id))
                {
                strSql += "  AND B.Brigada_ID = '" + brigada_id + "' ";
                }

              if (usuario_corte) {
                strSql += " AND A.Usuario_Corte = 1  ";
               }

            strSql += " ORDER BY EM.NOMBRE " +
                     ",EM.APELLIDO_PATERNO " +
                     ",EM.APELLIDO_MATERNO ";

            listadoEmpleados = Context.Query<EmpleadosOrdenTrabajoModel>().FromSql(strSql).ToList();     


            return listadoEmpleados;
        }

        public OrdenesTrabajoPaginado obtenerOrdenesPaginado(Cls_BusquedaOT_Model ModelBusqueda)
        {
            OrdenesTrabajoPaginado ordenesTrabajoPaginado = new OrdenesTrabajoPaginado();

            ordenesTrabajoPaginado.TotalElementos = obtenerOrdenesTrabajoTotal( ModelBusqueda);
            ordenesTrabajoPaginado.ListaOrdenes = obtenerOrdenesTrabajo( ModelBusqueda);

            return ordenesTrabajoPaginado;
        }

        public string Convertir_Fecha_Mexicana(string strFecha, bool agregarHoras) {
            DateTime datFecha;
            string fecha_cadena;
            datFecha=  DateTime.ParseExact(strFecha, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            if (agregarHoras)
            {
                fecha_cadena = datFecha.ToString("dd/MM/yyyy").ToString() + " 23:59:59 ";
               
            }
            else {
                fecha_cadena = datFecha.ToString("dd/MM/yyyy").ToString() + " 00:00:00 ";
            }

            return fecha_cadena;

        }

        public List<OrdenTrabajoViewModel> obtenerOrdenesTrabajo(Cls_BusquedaOT_Model ModelBusqueda)
        {
            int offset = 0;
            int renglonesPorPagina = 20;

            offset = (ModelBusqueda.Pagina - 1) * renglonesPorPagina;

            List<OrdenTrabajoViewModel> ListaOrdenesTrabajo = new List<OrdenTrabajoViewModel>();

            strSql = "SELECT OT.No_Orden_Trabajo " +
                     "   ,OT.Folio " +
                     "   ,OT.Estatus " +
                     "   ,CONVERT( VARCHAR, OT.Fecha, 103) + ' ' + CONVERT(VARCHAR, OT.Fecha, 108) as [Fecha_Elaboro] " +
                     "   ,ISNULL(P.No_Cuenta, '') as [No_Cuenta] " +
                     "   ,C.Nombre as [Colonia] " +
                     "   ,OT.Tipo_Falla_ID " +
                     "   ,TF.Descripcion as [Falla] " +
                     "   ,ISNULL(OT.No_Suspension, '') as [No_Suspension] " +
                     "   ,ISNULL(OT.Predio_ID, '') as  [Predio_ID] " +
                     "   ,B.Clave_Brigada + ' ' + B.Nombre_Brigada as [Clave_Nom] " +
                     "   ,ISNULL( UPPER(U.Apellido_Paterno + ' ' + U.Apellido_Materno + ' ' + U.Nombre),'') as [Usuario] " +
                     "   ,CA.Nombre + ' ' + ISNULL(OT.Numero, '') + ', ' + C.Nombre as [Domicilio] " +
                     "   , OT.Brigada_ID " +
                     "   , OT.Distrito_ID " +
                     "   ,D.NOMBRE as [Distrito] "+
                     "   , isnull( OT.Observaciones, '') as [Observaciones]" +
                     "   ,ISNULL(P.RPU, '') as  [RPU] " +
                     "   ,isnull(( " +
                     "   SELECT TOP 1 M.No_Medidor " +
                     "   FROM Cat_Cor_Predios_Medidores PM " +
                     "   JOIN Cat_Cor_Medidores M ON M.Medidor_ID = PM.Medidor_ID " +
                     "   WHERE PM.Predio_ID = OT.Predio_ID " +
                     "   AND M.Estatus = 'ACTIVO' " +
                     "   ORDER BY M.Fecha_Creo DESC " +
                     "   ),'')  as [No_Medidor] " +
                     " , ISNULL( P.latitud,0) as [latitud] " +
                     " ,isnull(P.longitud,0) as [longitud] " +
                     " ,OT.Calle_ID " +
                     " ,OT.Colonia_ID " +
                     " ,ISNULL(( " +
                     " SELECT TOP 1 MR.Nombre " +
                     " FROM Cat_Cor_Predios_Medidores PM " +
                     " JOIN Cat_Cor_Medidores M ON M.Medidor_ID = PM.Medidor_ID " +
                     " JOIN Cat_Com_Marcas MR ON MR.Marca_ID = M.Marca_ID " +
                     " WHERE PM.Predio_ID = OT.Predio_ID " +
                     "  AND M.Estatus = 'ACTIVO' " +
                     " ORDER BY M.Fecha_Creo DESC " +
                     " ),'' ) as [Marca] " +
                     " ,ISNULL(( " +
                     " SELECT TOP 1 l.Lectura_Facturacion " +
                     " FROM Cat_Cor_Medidores_Lecturas_Detalles l " +
                     " JOIN Ope_Cor_Facturacion_Recibos r ON l.Medidor_Detalle_ID = r.Medidor_Detalle_ID " +
                     " WHERE l.Predio_ID = OT.Predio_ID " +
                     " ORDER BY l.Anio DESC " +
                     "  ,l.Bimestre DESC " +
                     "  ,l.Fecha_Lectura DESC " +
                     "  ), 0) Ultima_Lectura " +
                     " ,OT.Usuario_Creo as [Autorizo] " +
                     " ,isnull( convert(varchar,Fecha_Trabajo,103),'') as Fecha_Trabajo " +
                     " ,isnull(Observaciones_Trabajo_Realizado,'') as Observaciones_Trabajo_Realizado " +
                     " ,ISNULL( OT.Usuario_Modifico,'') as Recibio " +
                     " ,isnull(Reporto,'') as Reporto " +
                     " ,isnull(Material_Fuga_ID,0)  as Material_Fuga_ID " +
                     " ,isnull(Diametro_Fuga_ID,0) as Diametro_Fuga_ID " +
                     ", isnull( convert(varchar,Fecha_Inicio,103),'') as Fecha_Inicio " +
                     ", isnull( convert(varchar,Fecha_Termino,103),'') as Fecha_Termino " +
                    "FROM OPE_COR_ORDENES_TRABAJO OT " +
                    "JOIN Cat_Cor_Brigadas B ON B.Brigada_ID = OT.Brigada_ID " +
                    "JOIN Cat_Cor_Distritos D ON D.Distrito_ID = OT.Distrito_ID " +
                    "JOIN Cat_Cor_Colonias C ON C.Colonia_ID = OT.Colonia_ID " +
                    "JOIN Cat_Cor_Calles CA ON CA.Calle_ID = OT.Calle_ID " +
                    "JOIN Cat_Cor_Tipos_Fallas TF ON TF.Tipo_Falla_ID = OT.Tipo_Falla_ID " +
                    "LEFT JOIN Cat_Cor_Predios P ON P.Predio_ID = OT.Predio_ID " +
                    "LEFT JOIN Cat_Cor_Usuarios U ON U.Usuario_ID = P.Usuario_ID " +
                    "WHERE 1 = 1 ";


            strSql += "   AND OT.No_Orden_Trabajo_Inicial IS NULL  ";

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Estatus))
            {
                strSql += " AND OT.Estatus = '" + ModelBusqueda.Estatus + "'  ";
            }

            strSql += "    AND ( " +
              "        SELECT ISNULL(SEGUIMIENTO_OT, 0) " +
              "        FROM Cat_Empleados  " +
              "        WHERE  Empleado_ID = '" + ModelBusqueda.Empleado_ID + "' " +
              "        ) = 1 ";

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Fecha_Inicio))
            {

                strSql += " AND OT.fecha >= '" + Convertir_Fecha_Mexicana(ModelBusqueda.Fecha_Inicio, false) + "' ";
            }

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Fecha_Fin))
            {

                strSql += " AND OT.fecha <= '" + Convertir_Fecha_Mexicana(ModelBusqueda.Fecha_Fin, true) + "' ";
            }


            if ( !String.IsNullOrWhiteSpace(ModelBusqueda.Query)) {
                strSql += " AND ( OT.Folio LIKE '%" + ModelBusqueda.Query.Trim() + "%' OR  p.RPU LIKE '%" + ModelBusqueda.Query.Trim() + "%' Or p.No_Cuenta LIKE '%" + ModelBusqueda.Query.Trim().Trim() + "%') ";
             }


             if (!String.IsNullOrWhiteSpace(ModelBusqueda.Distrito_ID)) {
                 strSql += "AND OT.Distrito_ID = '" + ModelBusqueda.Distrito_ID + "'";
             }

             if (!String.IsNullOrWhiteSpace(ModelBusqueda.Brigada_ID))
             {
                 strSql += " AND OT.Brigada_ID = '" + ModelBusqueda.Brigada_ID + "'";
             }

             if (!String.IsNullOrWhiteSpace(ModelBusqueda.Falla_ID))
             {
                 strSql += " AND OT.Tipo_Falla_ID = '" + ModelBusqueda.Falla_ID + "'";
             }

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Empleado_Asignado_ID))
            {
                strSql += " AND EXISTS ( " +
                    "  SELECT 1 " +
                    "  FROM Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados EA " +
                    "  WHERE EA.Empleado_ID = '" + ModelBusqueda.Empleado_Asignado_ID + "' " +
                    "   AND EA.No_Orden_Trabajo = OT.No_Orden_Trabajo" +
                    " ) ";
            }

            strSql +=  " ORDER BY Fecha DESC ";

            strSql += " OFFSET "+ offset +" ROWS  ";
            strSql += " FETCH Next "+ renglonesPorPagina +" ROWS ONLY ";

            ListaOrdenesTrabajo = Context.Query<OrdenTrabajoViewModel>().FromSql(strSql)
                .ToList();

           // ListaOrdenesTrabajo= Context.ExecuteQuery<OrdenTrabajoViewModel>(strSql);

            return ListaOrdenesTrabajo;
        }

        public int obtenerOrdenesTrabajoTotal(Cls_BusquedaOT_Model ModelBusqueda)
        {
            int resultado = 0;
            Cls_Totales cls_Totales = new Cls_Totales();
           

            strSql = "select  count(*) as resultado " +
             "FROM OPE_COR_ORDENES_TRABAJO OT " +
             "JOIN Cat_Cor_Brigadas B ON B.Brigada_ID = OT.Brigada_ID " +
             "JOIN Cat_Cor_Distritos D ON D.Distrito_ID = OT.Distrito_ID " +
             "JOIN Cat_Cor_Colonias C ON C.Colonia_ID = OT.Colonia_ID " +
             "JOIN Cat_Cor_Calles CA ON CA.Calle_ID = OT.Calle_ID " +
             "JOIN Cat_Cor_Tipos_Fallas TF ON TF.Tipo_Falla_ID = OT.Tipo_Falla_ID " +
             "LEFT JOIN Cat_Cor_Predios P ON P.Predio_ID = OT.Predio_ID " +
             "LEFT JOIN Cat_Cor_Usuarios U ON U.Usuario_ID = P.Usuario_ID " +
             "WHERE 1 = 1 ";

            strSql += "   AND OT.No_Orden_Trabajo_Inicial IS NULL  ";

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Estatus))
            {
                strSql += " AND OT.Estatus = '" + ModelBusqueda.Estatus + "'  ";
            }


            strSql += "    AND ( " +
                 "        SELECT ISNULL(SEGUIMIENTO_OT, 0) " +
                 "        FROM Cat_Empleados  " +
                 "        WHERE  Empleado_ID = '" + ModelBusqueda.Empleado_ID + "' " +
                 "        ) = 1 ";


            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Fecha_Inicio)) {

                strSql += " AND OT.fecha >= '"  + Convertir_Fecha_Mexicana(ModelBusqueda.Fecha_Inicio,false) + "' "; 
            }

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Fecha_Fin))
            {

                strSql += " AND OT.fecha <= '" + Convertir_Fecha_Mexicana(ModelBusqueda.Fecha_Fin, true) + "' ";
            }

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Query))
            {
                strSql += " AND ( OT.Folio LIKE '%" + ModelBusqueda.Query.Trim() + "%' OR  p.RPU LIKE '%" + ModelBusqueda.Query.Trim() + "%' Or p.No_Cuenta LIKE '%" + ModelBusqueda.Query.Trim().Trim() + "%') ";
            }


            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Distrito_ID))
            {
                strSql += "AND OT.Distrito_ID = '" + ModelBusqueda.Distrito_ID + "'";
            }


            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Brigada_ID))
            {
                strSql += " AND OT.Brigada_ID = '" + ModelBusqueda.Brigada_ID + "'";
            }

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Falla_ID))
            {
                strSql += " AND OT.Tipo_Falla_ID = '" + ModelBusqueda.Falla_ID + "'";
            }

            if (!String.IsNullOrWhiteSpace(ModelBusqueda.Empleado_Asignado_ID))
            {
                strSql += " AND EXISTS ( " +
                    "  SELECT 1 " +
                    "  FROM Ope_Cor_Asignacion_Ordenes_Trabajo_Empleados EA " +
                    "  WHERE EA.Empleado_ID = '" + ModelBusqueda.Empleado_Asignado_ID + "' " +
                    "   AND EA.No_Orden_Trabajo = OT.No_Orden_Trabajo" +
                    " ) ";
            }

            cls_Totales = Context.Query<Cls_Totales>().FromSql(strSql).FirstOrDefault();

            if (cls_Totales != null)
                resultado = cls_Totales.resultado;

            return resultado;
        }

        public CoordenadasOrdenesModel Obtener_Coordenas_Trabajo(string No_Orden_Trabajo)
        {
            CoordenadasOrdenesModel coordenadas = new CoordenadasOrdenesModel();

            strSql = "SELECT  No_Orden_Trabajo, isnull(Longitud_Trabajo,0) as Longitud_Trabajo,isnull(Latitud_Trabajo,0) as Latitud_Trabajo  FROM OPE_COR_ORDENES_TRABAJO WHERE No_Orden_Trabajo='" + No_Orden_Trabajo + "'";

            coordenadas = Context.Query<CoordenadasOrdenesModel>().FromSql(strSql).FirstOrDefault();

            return coordenadas;
        }

        public OrdenOTSeguimientoModel Obtener_Orden_Trabajo_Seguimiento(string No_Orden_Trabajo)
        {
            OrdenOTSeguimientoModel ordenOTSeguimientoModel = new OrdenOTSeguimientoModel();

            strSql = "SELECT Brigada_ID, Distrito_ID,Tipo_Falla_ID, isnull(Reporto,'') as Reporto,isnull( convert(varchar,Fecha_Trabajo,103),'') as Fecha_Trabajo, ISNULL( Usuario_Modifico,'') as Recibio, Estatus  FROM OPE_COR_ORDENES_TRABAJO WHERE No_Orden_Trabajo='" + No_Orden_Trabajo + "'";


            ordenOTSeguimientoModel = Context.Query<OrdenOTSeguimientoModel>().FromSql(strSql).FirstOrDefault();

            return ordenOTSeguimientoModel;
        }

        public List<Cls_Combo> Obtener_Estatus()
        {
            List<Cls_Combo> listaEstatus = new List<Cls_Combo>();

            Cls_Combo combo;

            combo = new Cls_Combo();
            combo.ID = "ASIGNADA";
            combo.Name = "ASIGNADA";

            listaEstatus.Add(combo);

            combo = new Cls_Combo();
            combo.ID = "ATENDIDA";
            combo.Name = "ATENDIDA";

            listaEstatus.Add(combo);

            combo = new Cls_Combo();
            combo.ID = "TERMINADA";
            combo.Name = "TERMINADA";

            listaEstatus.Add(combo);

            return listaEstatus;
        }

        public string Create_Orden_Trabajo(OrdenCreateModel ordenCreateModel)
        {
            string respuesta = "";
            CAT_COR_TIPOS_FALLAS entidadTiposFalla = Context.CAT_COR_TIPOS_FALLAS.Where(x => x.TIPO_FALLA_ID == ordenCreateModel.Tipo_Falla_ID).FirstOrDefault();

            ordenCreateModel.Brigada_ID = entidadTiposFalla.BRIGADA_ID;
            ordenCreateModel.Distrito_ID = entidadTiposFalla.DISTRITO_ID;

            SqlParameter sqlParametroPredioID = new SqlParameter("@Predio_ID", SqlDbType.Char, 10);

            if (ordenCreateModel.Predio_ID.Length > 0)
                sqlParametroPredioID.Value = ordenCreateModel.Predio_ID;
            else
                sqlParametroPredioID.Value = DBNull.Value;

           SqlParameter sqlParametroSubFallaID = new SqlParameter("@Sub_Falla_ID", SqlDbType.Char, 5);
            sqlParametroSubFallaID.Value = DBNull.Value;

            SqlParameter sqlParametroBrigadaID = new SqlParameter("@Brigada_ID", SqlDbType.Char, 5);
            sqlParametroBrigadaID.Value = ordenCreateModel.Brigada_ID;

            SqlParameter sqlParametroNoOrdenTrabajoInicial = new SqlParameter("@No_Orden_Trabajo_Inicial", SqlDbType.Char, 10);
            sqlParametroNoOrdenTrabajoInicial.Value = DBNull.Value;

            SqlParameter sqlParametroFecha = new SqlParameter("@Fecha", SqlDbType.DateTime);
            sqlParametroFecha.Value = DateTime.Now;

            SqlParameter sqlParametroBacheo = new SqlParameter("@Bacheo", SqlDbType.Char, 1);
            sqlParametroBacheo.Value = "";
            
            SqlParameter sqlParametroSupervisorDistrito = new SqlParameter("@Supervisor_Distrito", SqlDbType.VarChar,100);
            sqlParametroSupervisorDistrito.Value = DBNull.Value;

            SqlParameter sqlParametroEstatus = new SqlParameter("@Estatus", SqlDbType.VarChar, 20);
            sqlParametroEstatus.Value = "REGISTRADA";

            SqlParameter sqlParametroEmpleadoID = new SqlParameter("@Empleado_ID", SqlDbType.Char, 10);
            sqlParametroEmpleadoID.Value = ordenCreateModel.Empleado_ID;

            SqlParameter sqlParametroNoCuenta = new SqlParameter("@No_Cuenta", SqlDbType.Char, 20);

            if (ordenCreateModel.No_Cuenta.Length > 0)
                sqlParametroNoCuenta.Value = ordenCreateModel.No_Cuenta;
            else
                sqlParametroNoCuenta.Value = "";

            SqlParameter sqlParametroBrigadaIDPropuesta = new SqlParameter("@Brigada_Propuesta_ID", SqlDbType.Char, 5);
            sqlParametroBrigadaIDPropuesta.Value = ordenCreateModel.Brigada_ID;

            SqlParameter sqlParametroTipo_FallaID = new SqlParameter("@Tipo_Falla_ID", SqlDbType.Char, 5);
            sqlParametroTipo_FallaID.Value = ordenCreateModel.Tipo_Falla_ID;

            SqlParameter sqlParametroCalleID = new SqlParameter("@Calle_ID", SqlDbType.Char, 5);
            sqlParametroCalleID.Value = ordenCreateModel.Calle_ID;

            SqlParameter sqlParametroColoniaID = new SqlParameter("@Colonia_ID", SqlDbType.Char, 5);
            sqlParametroColoniaID.Value = ordenCreateModel.Colonia_ID;

            SqlParameter sqlParametroCalleReferenciaID = new SqlParameter("@CALLE_REFERENCIA1_ID", SqlDbType.Char, 5);

            if (ordenCreateModel.Calle_Referencia_ID.Length > 0)
                sqlParametroCalleReferenciaID.Value = ordenCreateModel.Calle_Referencia_ID;
            else
                sqlParametroCalleReferenciaID.Value = DBNull.Value;


           SqlParameter sqlParametroCalleReferencia2ID = new SqlParameter("@CALLE_REFERENCIA2_ID", SqlDbType.Char, 5);

            if (ordenCreateModel.Calle_Referencia_ID2.Length > 0)
                sqlParametroCalleReferencia2ID.Value = ordenCreateModel.Calle_Referencia_ID2;
            else
                sqlParametroCalleReferencia2ID.Value = DBNull.Value;


            SqlParameter sqlParametroNumero = new SqlParameter("@Numero", SqlDbType.VarChar, 250);

            if (ordenCreateModel.Numero.Length > 0)
                sqlParametroNumero.Value = ordenCreateModel.Numero;
            else
                sqlParametroNumero.Value = "";

            SqlParameter sqlParametroReferencia = new SqlParameter("@Referencia", SqlDbType.VarChar, 100);
            sqlParametroReferencia.Value = ordenCreateModel.Referencia;

            SqlParameter sqlParametroReporto = new SqlParameter("@Reporto", SqlDbType.VarChar, 100);
            sqlParametroReporto.Value = ordenCreateModel.Reporto;

            SqlParameter sqlParametroDomicilioReporto = new SqlParameter("@Domicilio_Reporto", SqlDbType.VarChar, 100);
            sqlParametroDomicilioReporto.Value = ordenCreateModel.Domicilio_Referencia;

            SqlParameter sqlParametroTelefono = new SqlParameter("@Telefono", SqlDbType.VarChar, 100);
            sqlParametroTelefono.Value = ordenCreateModel.Telefono;
           

            SqlParameter sqlParametroMedidor = new SqlParameter("@Medidor", SqlDbType.VarChar, 20);
            sqlParametroMedidor.Value = ordenCreateModel.No_Medidor;

            SqlParameter sqlParametroCanal = new SqlParameter("@Canal", SqlDbType.VarChar, 20);
            sqlParametroCanal.Value = "distribucion";

            SqlParameter sqlParametroObservaciones = new SqlParameter("@Observaciones", SqlDbType.VarChar, 255);
            sqlParametroObservaciones.Value = ordenCreateModel.Observaciones;

            SqlParameter sqlParametroGiroID = new SqlParameter("@Giro_ID", SqlDbType.Char, 10);
            sqlParametroGiroID.Value = DBNull.Value;

            SqlParameter sqlParametroUsuarioCreo = new SqlParameter("@USUARIO_CREO", SqlDbType.VarChar, 100);
            sqlParametroUsuarioCreo.Value = ordenCreateModel.Nombre_Empleado;

            SqlParameter sqlParametroSubsecuente = new SqlParameter("@Subsecuente", SqlDbType.Char, 2);
            sqlParametroSubsecuente.Value = "NO";

            SqlParameter sqlParametroPrioridad = new SqlParameter("@Prioridad", SqlDbType.VarChar, 7);
            sqlParametroPrioridad.Value = "NORMAL";


            

       var res= Context.Query<Cls_OT>().FromSql("EXEC SP_Insertar_Orden_Trabajo_distribucion @Predio_ID," +
                 "@Sub_Falla_ID,@Brigada_ID,@No_Orden_Trabajo_Inicial,@Fecha,@Bacheo,@Supervisor_Distrito," +
                 "@Estatus,@Empleado_ID,@No_Cuenta,@Brigada_Propuesta_ID,@Tipo_Falla_ID,@Calle_ID," +
                 "@Colonia_ID,@CALLE_REFERENCIA1_ID,@CALLE_REFERENCIA2_ID,@Numero,@Referencia," +
                 "@Reporto,@Domicilio_Reporto,@Telefono,@Medidor,@Canal,@Observaciones,@Giro_ID,@USUARIO_CREO,@Subsecuente,@Prioridad",
                sqlParametroPredioID, sqlParametroSubFallaID, sqlParametroBrigadaID, sqlParametroNoOrdenTrabajoInicial,
                sqlParametroFecha, sqlParametroBacheo, sqlParametroSupervisorDistrito, sqlParametroEstatus,
                sqlParametroEmpleadoID, sqlParametroNoCuenta, sqlParametroBrigadaIDPropuesta, sqlParametroTipo_FallaID,
                sqlParametroCalleID,sqlParametroColoniaID, sqlParametroCalleReferenciaID, sqlParametroCalleReferencia2ID,
                sqlParametroNumero, sqlParametroReferencia, sqlParametroReporto, sqlParametroDomicilioReporto,
                sqlParametroTelefono, sqlParametroMedidor, sqlParametroCanal, sqlParametroObservaciones,
                sqlParametroGiroID, sqlParametroUsuarioCreo, sqlParametroSubsecuente, sqlParametroPrioridad).FirstOrDefault();

            if(res != null)
            {
                respuesta = res.no_orden_trabajo;
            }

            return respuesta;

        }

        public void Asignar_Ordenes_Trabajo(IEnumerable<OrderAsingadaModel> listaAsignacion)
        {
            string No_Orden_Trabajo="";
            string Usuario_Creo="";

            using (IDbContextTransaction transaction = Context.Database.BeginTransaction())
            {

                try
                {

                    foreach (OrderAsingadaModel item in listaAsignacion) {

                        Asignacion_OT_EmpleadoModel asignacion_OT_Empleado = new Asignacion_OT_EmpleadoModel();
                        asignacion_OT_Empleado.Empleado_ID = item.Empleado_ID;
                        asignacion_OT_Empleado.No_Orden_Trabajo = item.No_Orden_Trabajo;
                        asignacion_OT_Empleado.Usuario_Creo = item.Usuario_Creo;
                        asignacionOrdenesTrabajo_Repo.agregarAsignacionTrabajo(Context, asignacion_OT_Empleado);

                        No_Orden_Trabajo = item.No_Orden_Trabajo;
                        Usuario_Creo = item.Usuario_Creo;
                    }

                    actualizarAsignacionOT(Context, No_Orden_Trabajo, Usuario_Creo);

                    transaction.Commit();
                }
                catch (Exception ex) {
                    transaction.Rollback();
                }
            }
        }
    }
}
