﻿using Dominio.Context;
using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;

namespace Dominio.Repo
{
  public  class EmpleadoRepo : IEmpleadoRepo
    {
        public StoreContext Context;

        public EmpleadoRepo(StoreContext context)
        {
            Context = context;
        }

        public IQueryable<CAT_EMPLEADOS> CAT_EMPLEADOS => Context.CAT_EMPLEADOS;

        public void actualizarToken(string EMPLEADO_ID, string Token)
        {
            CAT_EMPLEADOS empleados = Context.CAT_EMPLEADOS.Find(EMPLEADO_ID);
            empleados.Token = Token;
            Context.Update(empleados);
            Context.SaveChanges();
        }

        public  int doLogin(string Usuario, string Password)
        {
            int returnValue = 0;

            returnValue = StoreContext.verificarEmpleado(Usuario, Password);

            return returnValue;
        }
    }
}
