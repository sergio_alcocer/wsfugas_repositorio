﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;

namespace Dominio.Repo
{
  public  class DistritosRepo : IDistritosRepo
    {
        public StoreContext Context;

        public DistritosRepo(StoreContext context)
        {
            Context = context;
        }

        public IQueryable<CAT_COR_DISTRITOS> CAT_COR_DISTRITOS => Context.CAT_COR_DISTRITOS;

        public List<Cls_Combo> Obtener_Distritos(string pEmpleadoID)
        {
            List<Cls_Combo> listaDistritos = new List<Cls_Combo>();

            listaDistritos = Context.CAT_COR_DISTRITOS.OrderBy(x => x.NOMBRE)
                            .Where(x=> x.Empleado_ID == pEmpleadoID)
                            .Select(x => new Cls_Combo
                            {  
                                ID = x.DISTRITO_ID,
                                Name = x.NOMBRE
                            }).ToList();

            return listaDistritos;
        }
    }
}
