﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Interfaces;
using Dominio.Entidades;
using System.Linq;
using Dominio.Context;
using Dominio.model;

namespace Dominio.Repo
{
   public class BrigadasRepo : IBrigadas
    {
        public StoreContext Context;

        public BrigadasRepo(StoreContext context) {
            Context = context;
        }

        public IQueryable<Cat_Cor_Brigadas> Cat_Cor_Brigadas => Context.Cat_Cor_Brigadas;

        public List<Cls_Combo> Obtener_Brigadas()
        {
            List<Cls_Combo> listaBrigradas = new List<Cls_Combo>();

            listaBrigradas = Context.Cat_Cor_Brigadas.OrderBy(x => x.Nombre_Brigada)
                .Select(x => new Cls_Combo
                {
                    ID = x.Brigada_ID,
                    Name = x.Clave_Brigada + " " + x.Nombre_Brigada
                }).ToList();


            return listaBrigradas;
        }

        public List<Cls_Combo> Obtener_Brigadas(string pDistritoID)
        {
            List<Cls_Combo> listaBrigradas = new List<Cls_Combo>();

            listaBrigradas = Context.Cat_Cor_Brigadas.OrderBy(x => x.Nombre_Brigada)
                .Where(x=> x.Distrito_ID == pDistritoID)
                .Select(x => new Cls_Combo
                {
                    ID = x.Brigada_ID,
                    Name = x.Clave_Brigada + " " + x.Nombre_Brigada
                }).ToList();


            return listaBrigradas;
        }
    }
}
