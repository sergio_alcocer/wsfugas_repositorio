﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;


namespace Dominio.Interfaces
{
  public  interface IDiametrosFuga
    {

        IQueryable<Cat_Cor_Diametro_Fugas> Cat_Cor_Diametro_Fugas { get; }

        List<Cls_Combo> Obtener_Diametros_Fugas();
        
    }
}
