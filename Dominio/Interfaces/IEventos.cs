﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Context;
using Dominio.model;

namespace Dominio.Interfaces
{
   public interface IEventos
    {
        void agregarEventos(StoreContext context, EventoModel eventoModel);
    }
}
