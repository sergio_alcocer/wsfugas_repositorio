﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;

namespace Dominio.Interfaces
{
   public interface IDatosUbicacionRepo
    {
        List<Cls_Combo> Obtener_Colonias();
        List<Cls_Combo> Obtener_Calles_Por_Colonia(string Colonia_ID);
    }
}
