﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;
namespace Dominio.Interfaces
{
   public interface IImagenesRepo
    {

        IQueryable<ope_cor_almacenamientos_imagenes> ope_cor_almacenamientos_imagenes { get; }

        void agregarImagen(FotoOTModel fotoOT, string ruta_http, string ubicacion_fisica);
        List<FotoOTModel> obtenerFotos(string no_orden_trabajo);
    }
}
