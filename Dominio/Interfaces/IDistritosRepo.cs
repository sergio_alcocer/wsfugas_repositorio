﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;

namespace Dominio.Interfaces
{
   public interface IDistritosRepo
    {
        IQueryable<CAT_COR_DISTRITOS> CAT_COR_DISTRITOS { get; }

        List<Cls_Combo> Obtener_Distritos(string pEmpleadoID);
    }
}
