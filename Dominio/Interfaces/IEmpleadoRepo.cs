﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;

namespace Dominio.Interfaces
{
   public interface IEmpleadoRepo
    {
        IQueryable<CAT_EMPLEADOS> CAT_EMPLEADOS { get;  }

        int doLogin(string Usuario, string Password);
        void actualizarToken(string EMPLEADO_ID, string Token);
    }
}
