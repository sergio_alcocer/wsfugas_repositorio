﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.model;

namespace Dominio.Interfaces
{
  public  interface IOrdenesTrabajoRepo
    {


        int obtenerOrdenesTrabajoTotal(Cls_BusquedaOT_Model ModelBusqueda);
        List<OrdenTrabajoViewModel> obtenerOrdenesTrabajo(Cls_BusquedaOT_Model ModelBusqueda);
        OrdenesTrabajoPaginado obtenerOrdenesPaginado(Cls_BusquedaOT_Model ModelBusqueda);

        List<EmpleadosOrdenTrabajoModel> obtenerEmpleadosOrdenTrabajo(string brigada_id, Boolean usuario_corte);
        void Actualizar_Coordenadas_Trabajo(CoordenadasOrdenesModel coordenadas);

        CoordenadasOrdenesModel Obtener_Coordenas_Trabajo(string No_Orden_Trabajo);
        OrdenOTSeguimientoModel Obtener_Orden_Trabajo_Seguimiento(string No_Orden_Trabajo);

        List<Cls_Combo> Obtener_Estatus();

        string Actualizar_Orden_Trabajo(OrdenOTUpdate ordenOTUpdate);

        string Create_Orden_Trabajo(OrdenCreateModel ordenCreateModel);

        void Asignar_Ordenes_Trabajo(IEnumerable<OrderAsingadaModel> listaAsignacion);
    }
}
