﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;


namespace Dominio.Interfaces
{
   public interface IDistritoEmpleado_Repo
    {
        IQueryable<Ope_Cor_Asignacion_Distritos_Empleados> Ope_Cor_Asignacion_Distritos_Empleados { get; }
        List<Cls_Combo> Obtener_Todos_Los_Empleados_Distritos();
        List<Cls_Combo> Obtener_Empleado_Por_Brigadas(string Brigada_ID);
        List<Cls_Combo> Obtener_Empleados_Por_Un_Distrito(string Distrito_ID);
        
    }
}
