﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;
using Dominio.Context;

namespace Dominio.Interfaces
{
   public interface IMaterialesOrdenes_Repo
    {

        IQueryable<OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES> OPE_COR_ORDENES_TRABAJO_DETALLES_MATERIALES { get; }

        void Agregar_Material(OrdenesMaterialViewModel ordenesMaterialView);
        void Eliminar_Material(string Ordenes_Trabajo_Detalles_Materiales_ID);
        List<OrdenesMaterialViewModel> Obtener_Material_Orden(string No_Orden_Trabajo);

        decimal Obtener_Costo_Orden(StoreContext context, string No_Orden_Trabajo);
    }
}
