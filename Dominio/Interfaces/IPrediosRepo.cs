﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.model;

namespace Dominio.Interfaces
{
   public interface IPrediosRepo
    {
        int Obtener_Total_Predios(BusquedaPredioModel busquedaPredioModel);
        List<PrediosViewModel> Obtener_Predios(BusquedaPredioModel busquedaPredioModel);
        PrediosPaginado Obtener_Predios_Paginado(BusquedaPredioModel busquedaPredioModel);
    }
}
