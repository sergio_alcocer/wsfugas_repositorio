﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;

namespace Dominio.Interfaces
{
  public  interface IMaterialeFuga
    {
        IQueryable<Cat_Cor_Material_Fugas> Cat_Cor_Material_Fugas { get; }
        List<Cls_Combo> Obtener_Materiales_Fuga();
    }
}
