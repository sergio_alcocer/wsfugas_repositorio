﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;

namespace Dominio.Interfaces
{
    public interface IVisitas_Repo
    {
        IQueryable<Ope_Cor_Visitas_Ordenes_Trabajo> Ope_Cor_Visitas_Ordenes_Trabajo { get;}

        void Agregar_Visita(VisitasViewModel visitasView);
        List<VisitasViewModel> Obtener_Visitas(string pNoOrdenTrabajo);
        void Eliminar_Visita(string pNoVistaTrabajo,string pUsuario);
    }
}
