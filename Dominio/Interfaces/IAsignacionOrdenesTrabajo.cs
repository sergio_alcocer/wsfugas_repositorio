﻿using Dominio.Context;
using Dominio.model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Interfaces
{
   public  interface IAsignacionOrdenesTrabajo
    {

        void agregarAsignacionTrabajo(StoreContext context , Asignacion_OT_EmpleadoModel asignacion_OT);
    }
}
