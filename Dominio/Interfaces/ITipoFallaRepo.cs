﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;


namespace Dominio.Interfaces
{
   public interface ITipoFallaRepo
    {
        IQueryable<CAT_COR_TIPOS_FALLAS> CAT_COR_TIPOS_FALLAS { get; }
        List<Cls_Combo> obtenerFallas();
        List<Cls_Combo> Obtener_Fallas_Por_Distrito(string Distrito_ID);
    }
}
