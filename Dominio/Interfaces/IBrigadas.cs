﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using System.Linq;
using Dominio.model;

namespace Dominio.Interfaces
{
  public  interface IBrigadas
    {
        IQueryable<Cat_Cor_Brigadas> Cat_Cor_Brigadas { get; }

        List<Cls_Combo> Obtener_Brigadas();
        List<Cls_Combo> Obtener_Brigadas(string pDistritoID);
    }
}
