﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Entidades;
using Dominio.model;
using System.Linq;


namespace Dominio.Interfaces
{
   public interface IMaterialesRepo
    {
        IQueryable<CAT_COM_PRODUCTOS> CAT_COM_PRODUCTOS { get; }

        List<MaterialesViewModel> Obtener_Materiales();
    }
}
