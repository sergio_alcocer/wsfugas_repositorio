﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Servicio.ViewModels;
using Dominio.Interfaces;
using Dominio.model;

namespace Servicio.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BusquedasController : ControllerBase
    {
        IBrigadas brigadasRepositorio;
        IDistritosRepo distritosRepositorio;
        ITipoFallaRepo tipoFallaRepositorio;
        IDistritoEmpleado_Repo distritoEmpleado_Repo;

        IDiametrosFuga diametrosFugaRepo;
        IMaterialeFuga materialeFugaRepo;
        IOrdenesTrabajoRepo ordenesTrabajoRepo;

        IDatosUbicacionRepo datosUbicacionRepo;

        public BusquedasController(IBrigadas brigadasRepositorio, IDistritosRepo distritosRepositorio, 
            ITipoFallaRepo tipoFallaRepositorio, IDistritoEmpleado_Repo distritoEmpleado_Repo,
            IDiametrosFuga diametrosFugaRepo, IMaterialeFuga materialeFugaRepo,
            IOrdenesTrabajoRepo ordenesTrabajoRepo, IDatosUbicacionRepo datosUbicacionRepo) {
            this.brigadasRepositorio = brigadasRepositorio;
            this.distritosRepositorio = distritosRepositorio;
            this.tipoFallaRepositorio = tipoFallaRepositorio;
            this.distritoEmpleado_Repo = distritoEmpleado_Repo;
            this.diametrosFugaRepo = diametrosFugaRepo;
            this.materialeFugaRepo = materialeFugaRepo;
            this.ordenesTrabajoRepo = ordenesTrabajoRepo;
            this.datosUbicacionRepo = datosUbicacionRepo;
        }


        [Route("ObtenerEmpleadosBrigada"), HttpGet]
        public ActionResult<List<Cls_Combo>> ObtenerEmpleadosBrigada(string Brigada_ID) {

            List<Cls_Combo> lista = new List<Cls_Combo>();

            try
            {
                lista = distritoEmpleado_Repo.Obtener_Empleado_Por_Brigadas(Brigada_ID);
            }
            catch (Exception ex) {

            }

            return lista;
        }

        [Route("ObtenerParametroTrabajoRealizado"), HttpGet]
        public ActionResult<ParametroTrabajoRealizado> ObtenerParametroTrabajoRealizado(string Empleado_ID, string No_Orden_Trabajo) {
            ParametroTrabajoRealizado ObjParametros = new ParametroTrabajoRealizado();

            try
            {
                OrdenOTSeguimientoModel ordenOTSeguimiento = ordenesTrabajoRepo.Obtener_Orden_Trabajo_Seguimiento(No_Orden_Trabajo);
                ObjParametros.ListaEmpleadosDistritos = distritoEmpleado_Repo.Obtener_Empleado_Por_Brigadas(ordenOTSeguimiento.Brigada_ID);
                ObjParametros.ListaDiametroFuga = diametrosFugaRepo.Obtener_Diametros_Fugas();
                ObjParametros.ListaMaterialesFuga = materialeFugaRepo.Obtener_Materiales_Fuga();
                ObjParametros.ListaAreas = brigadasRepositorio.Obtener_Brigadas(ordenOTSeguimiento.Distrito_ID);
                ObjParametros.ListaFallas = tipoFallaRepositorio.Obtener_Fallas_Por_Distrito(ordenOTSeguimiento.Distrito_ID);

            }
            catch (Exception ex) {

            }

            return Ok(ObjParametros);

        }

        [Route("ObtenerParametrosBusquedaOT"), HttpGet]
        public ActionResult<ParametrosBusquedaOT> ObtenerParametrosBusquedaOT(string empleadoID) {
            ParametrosBusquedaOT ObjParametros = new ParametrosBusquedaOT();

            try
            {
                //ObjParametros.ListaDistrito = distritosRepositorio.Obtener_Distritos(empleadoID);
                ObjParametros.ListaEstatus = ordenesTrabajoRepo.Obtener_Estatus();
                ObjParametros.ListaBrigadas = brigadasRepositorio.Obtener_Brigadas("00002");
                ObjParametros.ListaFallas = tipoFallaRepositorio.Obtener_Fallas_Por_Distrito("00002");
                ObjParametros.ListaDistritoEmpleados = distritoEmpleado_Repo.Obtener_Empleados_Por_Un_Distrito("00002");
             

            }
            catch (Exception ex) {

            }
            return Ok(ObjParametros);
        }

        [Route("obtenerParametrosCrearOT"), HttpGet]
        public ActionResult<ParametroCrearOT> ObtenerParametrosCrearOT() {
            ParametroCrearOT parametroCrearOT = new ParametroCrearOT();

            try
            {
                parametroCrearOT.ListaColonias = datosUbicacionRepo.Obtener_Colonias();
                parametroCrearOT.ListaFallas = tipoFallaRepositorio.Obtener_Fallas_Por_Distrito("00002");
            }
            catch (Exception ex) {

            }

            return parametroCrearOT;

        }

        [Route("obtenerCallesPorColonia"), HttpGet]
        public ActionResult<List<Cls_Combo>> ObtenerCallesPorColonia(string Colonia_ID) {
            List<Cls_Combo> listaColonias = new List<Cls_Combo>();

            try
            {

                listaColonias = datosUbicacionRepo.Obtener_Calles_Por_Colonia(Colonia_ID);
            }
            catch (Exception ex) {

            }


            return Ok(listaColonias);
        }

        
       
    }
}