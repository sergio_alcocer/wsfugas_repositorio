﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dominio.Interfaces;
using Dominio.model;
using Servicio.ViewModels;

namespace Servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdenesMaterialController : ControllerBase
    {
        IMaterialesOrdenes_Repo materialesOrdenes_Repo;

        public OrdenesMaterialController(IMaterialesOrdenes_Repo materialesOrdenes_Repo) {
            this.materialesOrdenes_Repo = materialesOrdenes_Repo;
        }

        [Route("eliminarMaterial"), HttpGet]
        public ActionResult<MensajeModel> EliminarMaterial(string Ordenes_Trabajo_Detalles_Materiales_ID)
        {

            MensajeModel mensajeModel = new MensajeModel();
            try
            {
                materialesOrdenes_Repo.Eliminar_Material(Ordenes_Trabajo_Detalles_Materiales_ID);
                mensajeModel.Estatus = "bien";
            }
            catch (Exception ex)
            {
                mensajeModel.Estatus = ex.Message;
            }

            return mensajeModel;
        }


        [Route("agregarMaterial"), HttpPost]
        public ActionResult<MensajeModel> AgregarMaterial(OrdenesMaterialViewModel ordenesMaterialView) {

            MensajeModel mensajeModel = new MensajeModel();
            try
            {
                materialesOrdenes_Repo.Agregar_Material(ordenesMaterialView);
                mensajeModel.Estatus = "bien";
            }
            catch (Exception ex) {
                mensajeModel.Estatus = ex.Message;
            }

            return mensajeModel;
        }

        [Route("obtenerMaterial"), HttpGet]
        public ActionResult<List<OrdenesMaterialViewModel>> ObtenerMaterial(string NoOrdenTrabajo)
        {
            List<OrdenesMaterialViewModel> materialViewModels = new List<OrdenesMaterialViewModel>();
            try
            {
                materialViewModels = materialesOrdenes_Repo.Obtener_Material_Orden(NoOrdenTrabajo);
            }
            catch (Exception ex) {

            }

            return Ok(materialViewModels);
        }

    }
}