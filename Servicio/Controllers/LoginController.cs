﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Servicio.ViewModels;
using Dominio.Interfaces;
using Dominio.Entidades;
using Dominio.Context;
using Microsoft.EntityFrameworkCore;

namespace Servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {

        private IEmpleadoRepo empleadoRepo;

        public LoginController(IEmpleadoRepo empleadoRepo)
        {
            this.empleadoRepo = empleadoRepo;
        }

        [Route("verificarUsuario"), HttpPost]
        public ActionResult<MensajeModel> verificarUsuario(LoginModel loginModel)
        {

            MensajeModel mensajeModel = new MensajeModel();
            

            try
            {
                var empleado = empleadoRepo.CAT_EMPLEADOS
                    .Include(x=> x.APL_CAT_ROLES)
                    .Where(x => x.NO_EMPLEADO == loginModel.Usuario &&
                StoreContext.verificarEmpleado(loginModel.Usuario, loginModel.Password) == 1).FirstOrDefault();


                if (empleado == null) {
                    mensajeModel.Estatus = "Usuario o Password Incorrecto";
                    return mensajeModel;
                }

                empleadoRepo.actualizarToken(empleado.EMPLEADO_ID, loginModel.token);
                mensajeModel.Estatus = "bien";
                mensajeModel.Dato1 = empleado.APELLIDO_PATERNO + " " + empleado.APELLIDO_MATERNO + " " + empleado.NOMBRE;
                mensajeModel.Dato2 = empleado.EMPLEADO_ID;

                if (empleado.APL_CAT_ROLES == null)
                    mensajeModel.Dato3 = "sin rol";
                else
                mensajeModel.Dato3 =   empleado.APL_CAT_ROLES.NOMBRE;


            }
            catch (Exception ex) {
                mensajeModel.Estatus = ex.Message;
            }



            return mensajeModel;
        }

    }
}