﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Servicio.ViewModels;
using Dominio.Interfaces;
using Dominio.model;

namespace Servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrediosController : ControllerBase
    {
        IPrediosRepo prediosRepo;

        public PrediosController(IPrediosRepo prediosRepo) {
            this.prediosRepo = prediosRepo;
        }


        [Route("obtenerPredios"), HttpPost]
        public ActionResult<PrediosPaginado> Obtener_Predios(BusquedaPredioModel busquedaPredioModel) {
            PrediosPaginado prediosPaginado = new PrediosPaginado();

            try
            {
                prediosPaginado = prediosRepo.Obtener_Predios_Paginado(busquedaPredioModel);
            }
            catch (Exception ex) {

            }


            return Ok(prediosPaginado);

        }
    }
}