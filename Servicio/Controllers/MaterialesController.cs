﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dominio.Interfaces;
using Dominio.model;

namespace Servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialesController : ControllerBase
    {
        IMaterialesRepo materialesRepo;

        public MaterialesController(IMaterialesRepo materialesRepo) {
            this.materialesRepo = materialesRepo;
        }

        [Route("obtenerMateriales"), HttpGet]
        public ActionResult<List<MaterialesViewModel>> Obtener_Materiales() {
            List<MaterialesViewModel> MaterialesViewModel = new List<MaterialesViewModel>();

            try
            {
                MaterialesViewModel = materialesRepo.Obtener_Materiales();
            }
            catch (Exception e) {

            }

            return Ok(MaterialesViewModel);
        }
    }
}