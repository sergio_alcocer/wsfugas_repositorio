﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dominio.Interfaces;
using Dominio.model;
using Servicio.ViewModels;

namespace Servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VisitasController : ControllerBase
    {
        IVisitas_Repo VisitasRepositorio;

        public VisitasController(IVisitas_Repo VisitasRepositorio) {
            this.VisitasRepositorio = VisitasRepositorio;
        }


        [Route("obtenerVisita"), HttpGet]
        public ActionResult<List<VisitasViewModel>> Obtener_Visita(string pNoOrdenTrabajo) {
            List<VisitasViewModel> visitasViewModels = new List<VisitasViewModel>();
            try
            {
                visitasViewModels = VisitasRepositorio.Obtener_Visitas(pNoOrdenTrabajo);
            }
            catch (Exception ex) {

            }

            return Ok(visitasViewModels);
        }

        [Route("eliminarVisita"), HttpGet]
        public ActionResult<MensajeModel> Eliminar_Visita(string pNoVisitaTrabajo, string pUsuario) {
            MensajeModel mensajeModel = new MensajeModel();

            try
            {
                VisitasRepositorio.Eliminar_Visita(pNoVisitaTrabajo, pUsuario);
                mensajeModel.Estatus = "bien";
            }
            catch (Exception ex) {
                mensajeModel.Estatus = ex.Message;
            }

            return Ok(mensajeModel);
        }

        [Route("agregarVisita"), HttpPost]
        public ActionResult<MensajeModel> Agregar_Visita(VisitasViewModel visitasView)
        {
            MensajeModel mensajeModel = new MensajeModel();
            try
            {
                VisitasRepositorio.Agregar_Visita(visitasView);
                mensajeModel.Estatus = "bien";
            }
            catch (Exception ex) {

                mensajeModel.Estatus = ex.Message;
            }
            return Ok(mensajeModel);
        }
    }
}