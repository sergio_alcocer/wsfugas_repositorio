﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Dominio.model;
using Servicio.ViewModels;
using System.IO;
using Dominio.Interfaces;



namespace Servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagenesController : ControllerBase
    {
        private readonly IConfiguration _config;
        private IImagenesRepo imagenesRepo;
        public string url_foto;

        public ImagenesController(IConfiguration config, IImagenesRepo imagenesRepo) {
            _config = config;
            this.imagenesRepo = imagenesRepo;
           
        }


        [Route("obtenerImagen"), HttpGet]
        public ActionResult<List<FotoOTModel>> Obtener_Imagenes(string No_Orden_Trabajo)
        {
            List<FotoOTModel> listaFoto = new List<FotoOTModel>();

            try
            {
                listaFoto = imagenesRepo.obtenerFotos(No_Orden_Trabajo);
            }
            catch (Exception ex) {

            }

            return Ok(listaFoto);

        }

        [Route("guardarImagen"), HttpPost]
        public ActionResult<MensajeModel> Guardar_Imagen(FotoOTModel model) {
            MensajeModel mensajeModel = new MensajeModel();

            try
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Imagenes", model.Nombre_Foto);
                byte[] bytes = Convert.FromBase64String(model.Cadena_Foto);
                System.IO.File.WriteAllBytes(path, bytes);

                url_foto = _config.GetValue<string>("AppIdentitySettings:url_foto");
                imagenesRepo.agregarImagen(model, url_foto, path);
                mensajeModel.Estatus = "bien";

            }
            catch (Exception ex) {
                mensajeModel.Estatus = ex.Message;
            }
            return Ok(mensajeModel);
        }
    }
}