﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Servicio.ViewModels;
using Dominio.Interfaces;
using Dominio.Entidades;
using Dominio.Context;
using Microsoft.EntityFrameworkCore;
using Dominio.model;

namespace Servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdenesTrabajoController : ControllerBase
    {
        private IOrdenesTrabajoRepo ordenesTrabajoRepo;
        private IBrigadas brigadasRepo;

        public OrdenesTrabajoController(IOrdenesTrabajoRepo ordenesTrabajoRepo, IBrigadas brigadasRepo) {
            this.ordenesTrabajoRepo = ordenesTrabajoRepo;
            this.brigadasRepo = brigadasRepo;
        }

        [Route("obtenerCoordenadasTrabajo"), HttpGet]
        public ActionResult<CoordenadasOrdenesModel> Obtener_Coordenas_Trabajo(string No_Orden_Trabajo)
        {
            CoordenadasOrdenesModel coordenadas = new CoordenadasOrdenesModel();
            try
            {
                coordenadas = ordenesTrabajoRepo.Obtener_Coordenas_Trabajo(No_Orden_Trabajo);
            }
            catch (Exception ex) {

            }

            return Ok(coordenadas);

        }

        [Route("actualizarCoordenadas"), HttpPost]
        public ActionResult<MensajeModel> Actualizar_Coordenadas(CoordenadasOrdenesModel coordenadas) {
            MensajeModel mensajeModel = new MensajeModel();
            try
            {
                ordenesTrabajoRepo.Actualizar_Coordenadas_Trabajo(coordenadas);
                mensajeModel.Estatus = "bien";
            }
            catch (Exception ex) {
                mensajeModel.Estatus = ex.Message;
            }

            return Ok(mensajeModel);
        }

        [Route("ObtenerEmpleadosOrdenes"), HttpGet]
        public ActionResult<EmpleadosOrdenTrabajoModel> ObtenerEmpleadosOrdenes(string Brigada_ID, bool blnUsuarioCorte = false) {
            List<EmpleadosOrdenTrabajoModel> listaEmpleados = new List<EmpleadosOrdenTrabajoModel>();

            try
            {
                listaEmpleados = ordenesTrabajoRepo.obtenerEmpleadosOrdenTrabajo(Brigada_ID, blnUsuarioCorte);
            }
            catch (Exception ex) {

            }

            return Ok(listaEmpleados);
        }


        [Route("modificarOrdenTrabajo"), HttpPost]
        public ActionResult<MensajeModel> Modificar_Orden_Trabajo(OrdenOTUpdate ordenOTUpdate)
        {
            MensajeModel mensajeModel = new MensajeModel();

            try
            {
                OrdenOTSeguimientoModel ordenOTSeguimiento = ordenesTrabajoRepo.Obtener_Orden_Trabajo_Seguimiento(ordenOTUpdate.No_Orden_Trabajo);

                if (ordenOTSeguimiento.Estatus == "TERMINADA") {

                    mensajeModel.Estatus = "La orden seleccionada ya esta terminada";
                    return Ok(mensajeModel);
                }

                mensajeModel.Estatus = ordenesTrabajoRepo.Actualizar_Orden_Trabajo(ordenOTUpdate);
            }

            catch (Exception ex) {
                mensajeModel.Estatus = ex.Message;
            }
            return Ok(mensajeModel);
        }

        [Route("obtenerOrdenesTrabajo"), HttpPost]
        public ActionResult<OrdenesTrabajoPaginado> obtenerOrdenesTrabajo(Cls_BusquedaOT_Model ModelBusqueda)
        {

            OrdenesTrabajoPaginado ordenesTrabajoPaginado = new OrdenesTrabajoPaginado();

            try
            {
                ordenesTrabajoPaginado = ordenesTrabajoRepo.obtenerOrdenesPaginado(ModelBusqueda);
            }
            catch (Exception ex) {

            }

            return Ok(ordenesTrabajoPaginado);
        }

        [Route("guardarOrdenTrabajo"), HttpPost]
        public ActionResult<MensajeModel> guardarOT(OrdenCreateModel createModel)
        {
            MensajeModel mensajeModel = new MensajeModel();
            try
            {
                string id = ordenesTrabajoRepo.Create_Orden_Trabajo(createModel);
                mensajeModel.Estatus = "bien";
                mensajeModel.Dato1 = id;
            }
            catch (Exception ex) {
                mensajeModel.Estatus = ex.Message;
            }

            return mensajeModel;
        }

        [Route("asignarOrdenesTrabajo"), HttpPost]
        public ActionResult<MensajeModel> asignarOT(IEnumerable<OrderAsingadaModel> listaAsignacion) {
            MensajeModel mensajeModel = new MensajeModel();

            try
            {
                string No_Orden_Trabajo = listaAsignacion.FirstOrDefault().No_Orden_Trabajo;


                OrdenOTSeguimientoModel ordenOTSeguimiento = ordenesTrabajoRepo.Obtener_Orden_Trabajo_Seguimiento(No_Orden_Trabajo);

                if (ordenOTSeguimiento.Estatus != "REGISTRADA")
                {
                    mensajeModel.Estatus = "La orden seleccionada ya no esta registrada";
                    return Ok(mensajeModel);
                }


                ordenesTrabajoRepo.Asignar_Ordenes_Trabajo(listaAsignacion);
                mensajeModel.Estatus = "bien";
            }
            catch (Exception ex) {
                mensajeModel.Estatus = ex.Message;
            }

            return mensajeModel;
        }
    }
}