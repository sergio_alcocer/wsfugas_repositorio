﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dominio.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Dominio.model;

namespace Servicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrigadasController : ControllerBase
    {
        IBrigadas brigadasRepositorio;

        public BrigadasController(IBrigadas brigadasRepositorio) {
            this.brigadasRepositorio = brigadasRepositorio;
        }


        [Route("ObtenerBrigadasPorDistritos"), HttpGet]
        public ActionResult<Cls_Combo> ObtenerBrigadasPorDistritos(string distrito_id) {

            List<Cls_Combo> lista = new List<Cls_Combo>();

            try
            {

                lista = brigadasRepositorio.Obtener_Brigadas(distrito_id);
            }
            catch (Exception ex) {

            }

            return Ok(lista);
        }

        [Route("ObtenerBrigadas"), HttpGet]
        public ActionResult<Cls_Combo> ObtenerBrigadas()
        {
            List<Cls_Combo> lista = new List<Cls_Combo>();
            try
            {
                lista = brigadasRepositorio.Obtener_Brigadas();
            }
            catch (Exception ex)
            {

            }

            return Ok(lista);
        }

    }
}