﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using AutoMapper;
using Dominio.Context;
using Dominio.Interfaces;
using Dominio.Repo;

namespace Servicio
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddMvcCore().AddJsonFormatters(j =>
            {
                j.ContractResolver = new DefaultContractResolver();
                j.Formatting = Formatting.Indented;
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", builder =>
                {
                    builder
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
                    .AllowCredentials();
                });
            });

            services.AddDbContextPool<StoreContext>(
                options => options.UseSqlServer(
                    Configuration.GetConnectionString("conexion"))
                );

            services.AddScoped<IEmpleadoRepo, EmpleadoRepo>();
            services.AddScoped<IOrdenesTrabajoRepo, OrdenesTrabajoRepo>();
            services.AddScoped<IBrigadas, BrigadasRepo>();
            services.AddScoped<IDistritosRepo, DistritosRepo>();
            services.AddScoped<ITipoFallaRepo, TipoFallaRepo>();
            services.AddScoped<IVisitas_Repo, Visita_Repo>();
            services.AddScoped<IMaterialesRepo, MaterialesRepo>();
            services.AddScoped<IMaterialesOrdenes_Repo, MaterialesOrdenes_Repo>();
            services.AddScoped<IDistritoEmpleado_Repo, DistritoEmpleado_Repo>();
            services.AddScoped<IDiametrosFuga, DiametroFuega_Repo>();
            services.AddScoped<IMaterialeFuga, MaterialFuga_Repo>();
            services.AddScoped<IAsignacionOrdenesTrabajo, AsignacionOrdenesTrabajo>();
            services.AddScoped<IEventos, Eventos_Repo>();
            services.AddScoped<IImagenesRepo, Imagenes_Repo>();
            services.AddScoped<IDatosUbicacionRepo, DatosUbicacionRepo>();
            services.AddScoped<IPrediosRepo, PrediosRepo>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = "SIMAPAG",
                    Version = "v1",
                    Description = "Service to support ",
                    TermsOfService = new Uri("https://www.simapag.gob.mx/"),
                    License = new OpenApiLicense
                    {
                        Name = "Freeware",
                        Url = new Uri("https://www.simapag.gob.mx/"),
                    }
                });
                // var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                // var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //  c.IncludeXmlComments(xmlPath);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // no se les olvide porner eso , porque si no va funcionar
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "Fuga Service v1");
            });
            app.UseStaticFiles();
            app.UseCors("AllowAll");
            app.UseMvc();
        }
    }
}
