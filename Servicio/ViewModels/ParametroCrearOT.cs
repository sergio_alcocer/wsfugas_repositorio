﻿using Dominio.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicio.ViewModels
{
    public class ParametroCrearOT
    {
        public List<Cls_Combo> ListaFallas { get; set; }
        public List<Cls_Combo> ListaColonias { get; set; }
    }
}
