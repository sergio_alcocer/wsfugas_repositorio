﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dominio.model;

namespace Servicio.ViewModels
{
    public class ParametrosBusquedaOT
    {
        public List<Cls_Combo> ListaBrigadas { get; set; }
        public List<Cls_Combo> ListaDistrito { get; set; }
        public List<Cls_Combo> ListaFallas { get; set; }
        public List<Cls_Combo> ListaDistritoEmpleados { get; set; }
        public List<Cls_Combo> ListaEstatus { get; set; }
    }
}
