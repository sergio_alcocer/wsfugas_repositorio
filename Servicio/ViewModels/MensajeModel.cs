﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicio.ViewModels
{
    public class MensajeModel
    {
        public string Estatus { get; set; }
        public string Dato1 { get; set; }
        public string Dato2 { get; set; }
        public string Dato3 { get; set; }

        public MensajeModel() {
            Estatus = "";
            Dato1 = "";
            Dato2 = "";
            Dato3 = "";

        }
    }
}
