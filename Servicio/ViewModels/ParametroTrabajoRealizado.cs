﻿using Dominio.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicio.ViewModels
{
    public class ParametroTrabajoRealizado
    {
        public List<Cls_Combo>  ListaDiametroFuga { get; set; }
        public List<Cls_Combo>  ListaMaterialesFuga { get; set; }
        public List<Cls_Combo>  ListaAreas { get; set; }
        public List<Cls_Combo> ListaEmpleadosDistritos { get; set; }
        public List<Cls_Combo> ListaFallas { get; set; }
    }
}
